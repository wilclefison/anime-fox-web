/*----------------------------------------------
Index Of Script
------------------------------------------------

:: Sticky Header Animation & Height
:: Back to Top
:: Header Menu Dropdown
:: Slick Slider
:: Owl Carousel
:: Page Loader
:: Mobile Menu Overlay
:: Equal Height of Tab Pane
:: Active Class for Pricing Table
:: Select 2 Dropdown
:: Video Popup
:: Flatpicker
:: Custom File Uploader

------------------------------------------------
Index Of Script
----------------------------------------------*/
// if (!window.BB_ind) { BB_ind = 0; } if (!window.BB_r) { BB_r = Math.floor(Math.random() * 1000000000) };

// function loadingAP() {
// 	BB.getAds(
// 		"Apequeno",  // -> div id 
// 		2013602, // -> placement id
// 		BB_ind++,
// 		true // -> this parameter defines if you want to clear the div before inserting or not.
// 	);
// }
// function loadingAP2() {
// 	BB.getAds(
// 		"Apequeno2",  // -> div id 
// 		2013615, // -> placement id
// 		BB_ind++,
// 		true // -> this parameter defines if you want to clear the div before inserting or not.
// 	);
// }
// function loadingAG(id) {
// 	BB.getAds(
// 		"AGrande"+id,  // -> div id 
// 		2013616, // -> placement id
// 		BB_ind++,
// 		true // -> this parameter defines if you want to clear the div before inserting or not.
// 	);
// }

// END Anuncios 


(function (jQuery) {
	"use strict";
	jQuery(document).ready(function () {

		function activaTab(pill) {
			jQuery(pill).addClass('active show');
		}

		/*---------------------------------------------------------------------
			Sticky Header Animation & Height
		----------------------------------------------------------------------- */
		function headerHeight() {
			var height = jQuery("#main-header").height();
			jQuery('.iq-height').css('height', height + 'px');
		}
		jQuery(function () {
			var header = jQuery("#main-header"),
				yOffset = 0,
				triggerPoint = 80;

			headerHeight();

			jQuery(window).resize(headerHeight);
			jQuery(window).on('scroll', function () {

				yOffset = jQuery(window).scrollTop();

				if (yOffset >= triggerPoint) {
					header.addClass("menu-sticky animated slideInDown");
				} else {
					header.removeClass("menu-sticky animated slideInDown");
				}

			});
		});

		/*---------------------------------------------------------------------
			Back to Top
		---------------------------------------------------------------------*/
		var btn = $('#back-to-top');
		$(window).scroll(function () {
			if ($(window).scrollTop() > 50) {
				btn.addClass('show');
			} else {
				btn.removeClass('show');
			}
		});
		btn.on('click', function (e) {
			e.preventDefault();
			$('html, body').animate({ scrollTop: 0 }, '300');
		});

		/*---------------------------------------------------------------------
			Header Menu Dropdown
			---------------------------------------------------------------------*/
		jQuery('[data-toggle=more-toggle]').on('click', function () {
			jQuery(this).next().toggleClass('show');
		});

		
		/*---------------------------------------------------------------------
				Page Loader
			----------------------------------------------------------------------- */
		jQuery("#load").fadeOut();
		jQuery("#loading").delay(0).fadeOut("slow");

		jQuery('.widget .fa.fa-angle-down, #main .fa.fa-angle-down').on('click', function () {
			jQuery(this).next('.children, .sub-menu').slideToggle();
		});


		/*---------------------------------------------------------------------
		Mobile Menu Overlay
		----------------------------------------------------------------------- */
		jQuery(document).on("click", function (event) {
			var $trigger = jQuery(".main-header .navbar");
			if ($trigger !== event.target && !$trigger.has(event.target).length) {
				jQuery('body').removeClass('nav-open');
				jQuery('#navbarSupportedContent').removeClass('show');
			}
		});
		jQuery(document).on("click", '.c-toggler', function () {
			jQuery('body').addClass('nav-open');
			jQuery('#navbarSupportedContent').addClass('show');
		});

		/*---------------------------------------------------------------------
		  Equal Height of Tab Pane
		-----------------------------------------------------------------------*/
		jQuery('.trending-content').each(function () {
			var highestBox = 0;
			jQuery('.tab-pane', this).each(function () {
				if (jQuery(this).height() > highestBox) {
					highestBox = jQuery(this).height();
				}
			});
			jQuery('.tab-pane', this).height(highestBox);
		});

		jQuery('body').on("click", ".playbtn", function () {
			jQuery('#myVideo').prop("volume", 1)
			if (jQuery('#myVideo').prop('muted')) {
				jQuery('#myVideo').prop('muted', false);
			} else {
				jQuery('#myVideo').prop('muted', true);
			}
		});


	});


})(jQuery);

import ApiService from "./ApiService";

export default class MovieService {

  static getMovie(nome, categorias = [], page = 1) {
    return ApiService.get("Anime", {
      params: {
        nome,
        page,
        categorias
      }
    })
  };

  static getMovie(id) {
    return ApiService.get("/api/movie/" + id);
  }

  static getImages() {
    return ApiService.get("Capa/");
  }

  static getNew() {
    return ApiService.get("Anime/new/");
  }

  static getRecents() {
    return ApiService.get("Anime/recentsUser/");
  }


  static getTop(categoria = '') {
    return ApiService.get("Anime/topAnimes/" + categoria);
  }


}
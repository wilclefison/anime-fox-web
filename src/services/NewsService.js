import ApiService from "./ApiService";

export default class NewsService {

    static getNews(nome) {
        return ApiService.get(`/api/post/${nome}`)
    };

    static savePositionView(link, position) {
        return ApiService.post("Link/saveProgress", {
            link,
            position
        })
    };

}
import ApiService from "./ApiService";

export default class HomeService {

    static getHome(start, limit) {
        return ApiService.get("/api/inicio", {
            params: {
                start,
                limit
            }
        })
    };

    static getSlides() {
        return ApiService.get("/api/slide")
    };

}
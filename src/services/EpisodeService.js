import ApiService from "./ApiService";

export default class EpisodeService {

  static getEpisode(id) {
    return ApiService.get(`/api/ep/${id}`)
  }

  static getEpiTemp(anime, temp) {
    return ApiService.get(`/api/episodio/${anime}/${temp}`)
  }

  static saveAccessEpisode(id) {
    return ApiService.post("Episode/saveAccessEpisode/" + id)
  }

}
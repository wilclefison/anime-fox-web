import ApiService from "./ApiService";

export default class BuscaService {

    static getBusca(nome) {
        return ApiService.get(`/api/busca/${nome}`)
    }


}
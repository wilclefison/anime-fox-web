import AnimeService from "./AnimeService";
import EpisodeService from "./EpisodeService";
import NewsService from "./NewsService";
import SignUpService from "./SignUpService";
import LoginService from "./LoginService";
import UserService from "./UserService";
import CategoriaService from "./CategoriaService";
import HomeService from './HomeServices';
import BuscaService from './BuscaService';
import MovieService from './MovieService';
const Api = {
  AnimeService,
  EpisodeService,
  NewsService,
  SignUpService,
  LoginService,
  UserService,
  CategoriaService,
  HomeService,
  MovieService,
  BuscaService
};

export default Api;
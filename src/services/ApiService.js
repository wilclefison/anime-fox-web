import axios from "axios";


const ApiService = axios.create({
    baseURL: process.env.urlApi
});

ApiService.interceptors.request.use(async (config) => {

    if (typeof window !== 'undefined') {
        const userToken = await localStorage.getItem("token");

        if (userToken) {
            config.headers = { Authorization: `Bearer ${userToken}` }
        }
    }

    return config;
});

ApiService.interceptors.response.use((response) => {
    return response.data;
}, (error) => {
    console.log(error)
    if (typeof window !== 'undefined' && error.response != undefined) {

        if (error.response.data && error.response.data.code && +error.response.data.code === 1001) {
            localStorage.removeItem('token');

            let uri = window.location.pathname;

            if (window.location.pathname) {
                window.location.href = '/login/#' + uri;
                return;
            }

            window.location.href = '/login';
        }


    }
    throw (error.response || error.request).data;


});

export default ApiService;
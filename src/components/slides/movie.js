import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';
import Api from "../../services/Api";

const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
    <div
        {...props}
        className={
            "slick-nav prev-arrow" +
            (currentSlide === 0 ? " slick-disabled" : "")
        }>
        <i></i>
        <svg><use xlinkHref="#circle" /></svg>
    </div>
);
const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
    <div
        {...props}
        className={
            "slick-nav next-arrow" +
            (currentSlide === slideCount - 1 ? " slick-disabled" : "")
        }>
        <i></i>
        <svg><use xlinkHref="#circle" /></svg>
    </div>
);

export default function MovieSlider(props) {
    const [slides, setSlides] = useState(props.homeItens);

    const settings = {
        centerMode: true,
        centerPadding: '200px',
        slidesToShow: 1,
        prevArrow: <SlickArrowLeft />,
        nextArrow: <SlickArrowRight />,
        arrows: true,
        dots: false,
        autoplay: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 1
                }
            }
        ]


    };

    return (
        <div id="home-slider" className="slider m-0 p-0" >
            <Slider {...settings} >
                <div>
                    <a href="/filmes/your-name">
                        <div className="shows-img">
                            <img src="https://image.tmdb.org/t/p/w780/mMtUybQ6hL24FXo0F3Z4j2KG7kZ.jpg" className="w-100" alt="" />
                            <div className="shows-content">
                                <h4 className="text-white mb-1">Your Name</h4>
                                <div className="movie-time d-flex align-items-center">
                                    <div className="badge badge-secondary p-1 mr-2">13+</div>
                                    <span className="text-white">1h 46m</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="/filmes/my-hero-academia-ascensao-dos-herois">
                        <div className="shows-img">
                            <img src="https://image.tmdb.org/t/p/w780/uicia399gyMGE1smatJ41M0CtFx.jpg" className="w-100" alt="" />
                            <div className="shows-content">
                                <h4 className="text-white mb-1">My Hero Academia: Heroes Rising</h4>
                                <div className="movie-time d-flex align-items-center">
                                    <div className="badge badge-secondary p-1 mr-2">13+</div>
                                    <span className="text-white">1h 36m</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="/filmes/made-in-abyss-fukaki-tamashii-no-reimei">
                        <div className="shows-img">
                            <img src="https://image.tmdb.org/t/p/w780/z0HQXKIZLpe10cKzG7oetRbxBIA.jpg" className="w-100" alt="" />
                            <div className="shows-content">
                                <h4 className="text-white mb-1">Made in Abyss: Fukaki Tamashii no Reimei</h4>
                                <div className="movie-time d-flex align-items-center">
                                    <div className="badge badge-secondary p-1 mr-2">13+</div>
                                    <span className="text-white">1h 45m</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </Slider>
        </div >
    )

}
import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';
import Api from "../../services/Api";

const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
    <div
        {...props}
        className={
            "slick-nav prev-arrow" +
            (currentSlide === 0 ? " slick-disabled" : "")
        }>
        <i></i>
        <svg><use xlinkHref="#circle" /></svg>
    </div>
);
const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
    <div
        {...props}
        className={
            "slick-nav next-arrow" +
            (currentSlide === slideCount - 1 ? " slick-disabled" : "")
        }>
        <i></i>
        <svg><use xlinkHref="#circle" /></svg>
    </div>
);

export default function HomeSlider(props) {
    const [slides, setSlides] = useState(props.homeItens);
   
    const settings = {
        autoplay: false,
        speed: 800,
        lazyLoad: 'progressive',
        arrows: true,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: <SlickArrowLeft />,
        nextArrow: <SlickArrowRight />,

    };

    const butonText = (v) => {
        switch (v) {
            case 2:
                return 'Leia Agora'
            case 3:
                return 'Assitir'
            default:
                return 'Assitir Agora';
        }
    }
    return (
        <div id="home-slider" className="slider m-0 p-0" >
            <Slider {...settings} >
                {slides.map((i) => (
                    <div key={i.id}>
                        <div className="slide slick-bg" style={i.type != 3 ? { backgroundImage: `url(${i.img})` } : {}} >
                            {i.type == 3 && <video muted autoPlay loop id="myVideo">
                                <source src={i.img} type="video/mp4" />
                            </video>}
                            <div className="container-fluid position-relative h-100" style={{ zIndex: 2 }}>
                                <div className="slider-inner h-100">
                                    <div className="row align-items-center h-100">
                                        <div className="col-xl-6 col-lg-12 col-md-12">
                                            <h1 className="slider-text big-title title ">{i.titulo}</h1>
                                            {/* <div className="d-flex align-items-center">
                                                <span className="badge badge-secondary p-2">18+</span>
                                                <span className="ml-3">2 Seasons</span>
                                            </div> */}
                                            <p>{i.descri}</p>
                                            <div className="d-flex align-items-center r-mb-23">
                                                <a href={`${i.link}`} className="btn btn-hover"><i className="fa fa-play mr-2" aria-hidden="true" />{butonText(i.type)}</a>
                                                {i.type == 1 && <a href={`${i.link}`} className="btn btn-link">Mais Informações</a>}
                                            </div>
                                        </div>
                                    </div>
                                    {i.type == 3 && <div className="trailor-video">
                                        <a className="video-unmuted playbtn" >
                                            <i className="ri-volume-mute-fill" />
                                        </a>
                                    </div>}
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </Slider>
        </div >
    )

}
import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';
import Api from "../../services/Api";
import Loader from 'react-loader-spinner';
import { AGrande } from '../Anuncios/anuncions'

const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
    <div
        {...props}
        className={
            "slick-nav prev-arrow" +
            (currentSlide === 0 ? " slick-disabled" : "")
        }>
        <i></i>
        <svg><use xlinkHref="#circle" /></svg>
    </div>
);
const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
    <div
        {...props}
        className={
            "slick-nav next-arrow" +
            (currentSlide === slideCount - 1 ? " slick-disabled" : "")
        }>
        <i></i>
        <svg><use xlinkHref="#circle" /></svg>
    </div>
);


export default function ItensSlider(props) {
    const [content, setContent] = useState(props.homeItens);
    const [pagina, setPage] = useState(3);
    const [isFetching, setIsFetching] = useState(false);

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    useEffect(() => {
        if (!isFetching) return;
        loadCategoria();
    }, [isFetching]);


    const settings = {
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        autoplay: false,
        slidesToShow: 5,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: false
                }
            }
        ],
        prevArrow: <SlickArrowLeft />,
        nextArrow: <SlickArrowRight />,
    };

    const loadCategoria = async () => {
        try {
            if (pagina <= 21) {
                const responseHomeItens = await Api.HomeService.getHome(pagina, 3);
                setPage(pagina + 3)
                setContent(prevState => ([...prevState, ...responseHomeItens]));
            } else {
                window.removeEventListener('scroll', handleScroll);
            }
        } catch (e) {
            console.log(e)
        } finally {
            setIsFetching(false);
        }
    }

    const handleScroll = () => {
        const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight) - 15;
        const windowBothrefm = windowHeight + window.pageYOffset;
        if (windowBothrefm >= docHeight) {
            setIsFetching(true);
        }
    }


    return (
        <div>
            {content.map((i, index) => (
                <section id="iq-favorites" key={index}>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-12 overflow-hidden">
                                <div className="iq-main-header d-flex align-items-center justify-content-between">
                                    <h4 className="main-title">{i.categoria}</h4>
                                    {i.categoria !== 'Novos Episódios' &&
                                        <a href={i.categoria !== 'Novos Episódios' && i.categoria !== 'Notícias' ? `/categoria/${i.categoria}` : `/noticias`} className="text-primary">Ver Todos</a>
                                    }
                                </div>
                                <div className="favorites-contens">
                                    <Slider {...settings} className="favorites-slider list-inline row p-0 mb-0" >
                                        {i.tipo == 1 && i.conteudo.map(ic => (
                                            <li className="slide-item" key={`${i.categoria}-${ic.id}-ultep`}>
                                                <a href={`/assitir/${ic.url}/${ic.id}`}>
                                                    <div className="block-images position-relative">
                                                        <div className="img-box">
                                                            <img src={ic.img} className="img-fluid" alt="" />
                                                        </div>
                                                        <div className="block-description">
                                                            <h6>{ic.nome}</h6>
                                                            <div className="movie-time d-flex align-items-center my-2">
                                                                <div className="badge badge-secondary p-1 mr-2">13+</div>
                                                                <span className="text-white"></span>
                                                            </div>
                                                            <div className="hover-buttons">
                                                                <span className="btn btn-hover">
                                                                    <i className="fa fa-play mr-1" aria-hidden="true" />Assistir Agora</span>
                                                            </div>
                                                        </div>
                                                        <div className="block-social-info">
                                                            <ul className="list-inline p-0 m-0 music-play-lists">
                                                                <li><span><i className="ri-volume-mute-fill" /></span></li>
                                                                <li><span><i className="ri-heart-fill" /></span></li>
                                                                <li><span><i className="ri-add-line" /></span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="text-center">
                                                        <h6>{ic.ep.includes('Episódio') ? ic.ep : `E${ic.numero} - ${ic.ep}`}</h6>
                                                    </div>
                                                </a>
                                            </li>
                                        ))}
                                        {i.tipo == 3 && i.conteudo.map(nt => (
                                            <li className="slide-item" key={nt.id}>
                                                <a href={`post/${nt.link}`}>
                                                    <div className="block-images position-relative">
                                                        <div className="img-box">
                                                            <img src={nt.img} alt={nt.titulo} className="img-fluid" alt="" />
                                                        </div>
                                                        <div className="block-description">
                                                            <div className="hover-buttons">
                                                                <span className="btn btn-hover">
                                                                    <i className="fa fa-play mr-1" aria-hidden="true" />Ler Agora</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="text-center">
                                                        <h6>{nt.titulo}</h6>
                                                    </div>
                                                </a>
                                            </li>
                                        ))}
                                        {i.tipo == 2 && i.conteudo.map(conteudo => (
                                            <li className="slide-item" key={`${i.categoria}-${conteudo.nome}`}>
                                                <a href={`/anime/${conteudo.url}`}>
                                                    <div className="block-images position-relative">
                                                        <div className="img-box">
                                                            <img src={`https://image.tmdb.org/t/p/w300/${conteudo.img}`} className="img-fluid" alt="" />
                                                        </div>
                                                        <div className="block-description">
                                                            <h6>{conteudo.nome}</h6>
                                                            <div className="movie-time d-flex align-items-center my-2">
                                                                <div className="badge badge-secondary p-1 mr-2">13+</div>
                                                                <span className="text-white"></span>
                                                            </div>
                                                            <div className="hover-buttons">
                                                                <span className="btn btn-hover">
                                                                    <i className="fa fa-play mr-1" aria-hidden="true" />Assistir Agora</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        ))}
                                    </Slider>
                                </div>
                            </div>
                        </div>
                    </div>
                    <AGrande id={index} />
                </section>
            ))}
            {isFetching && <div style={{ width: "100%", height: "100", display: "flex", justifyContent: "center", alignItems: "center" }} >
                <Loader type="ThreeDots" color="#2BAD60" height={100} width={100} />
            </div>}
        </div>
    )

}
import React from 'react'
import Loader from 'react-loader-spinner'
import Head from 'next/head'
import { AGrande } from '../Anuncios/anuncions'




export default class MovieList extends React.Component {
    state = {
        content: this.props.data.data.conteudo,
        page: 24,
        totalPages: this.props.data.data.contagem,
        loadingScroll: false
    }
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }
    loadMovie = async () => {
        const catId = this.props.data.pid;
        if (this.state.page <= this.state.totalPages) {
            fetch(`${process.env.urlApi}api/movie?start=${this.state.page}&limit=24`)
                .then(res => res.json())
                .then((data) => {
                    this.setState({ content: this.state.content.concat(data.conteudo), page: this.state.page + 24, loadingScroll: false })
                }).catch(console.log);
        }
    }
    handleScroll = () => {
        const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        const windowBottom = windowHeight + window.pageYOffset + 80;

        if (windowBottom >= docHeight - 1) {
            if (this.state.page <= this.state.totalPages) {
                this.setState({ loadingScroll: true })
                this.loadMovie()
            }

        }
    }

    render() {
        const { content, loadingScroll } = this.state

        return (
            <div className="main-content">
                <section className="movies">
                    <div className="container-fluid">
                        <div className="list container-items">
                            <div className="col-sm-12 overflow-hidden">
                                <div className="iq-main-header d-flex align-items-center justify-content-between">
                                    <h4 className="main-title">Últimos Filmes</h4>
                                </div>
                            </div>
                            <span className="animList">
                                {
                                    content.map((conteudo, index) => (
                                        <div className="list-item boxart-size-16x9 " key={index}>
                                            <div className="content content-vertical">
                                                <div className="poster poster-vertical">
                                                    <a href={`/filmes/${conteudo.url}`} >
                                                        <div className="poster-text">{conteudo.nome}</div>
                                                        <div className="poster-image poster-shadow boxart-rounded" style={{
                                                            backgroundImage: `url(${conteudo.img})`,
                                                            backgroundPosition: 'center',
                                                            backgroundSize: 'cover',
                                                            backgroundRepeat: 'no-repeat'
                                                        }}><span className="poster-desc">
                                                                {conteudo.nome}
                                                            </span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }

                            </span>
                        </div>
                    </div>
                    {loadingScroll && <div style={{ width: "100%", height: "100", display: "flex", justifyContent: "center", alignItems: "center" }} >
                        <Loader type="ThreeDots" color="#2BAD60" height={100} width={100} />
                    </div>}
                </section>
                <Head>
                    <title>{`Anime Fox - Filmes`}</title>
                    <meta property="og:url" content="http://animefox.me/" />
                    <meta property="og:type" content="article" />
                    <meta property="og:title" content="Anime Fox" />
                    <meta property="og:description" content="Assista Naruto Shippuden, Attack on Titan, Dragon Ball Super, e muito mais! Acompanhe suas séries favoritas, com legendas profissionais em português. Anime. Novos Episódios do Japão. Mais de 1000 Séries. Legendas em Português." />
                    <meta property="og:image" content="http://animefox.me/images/ogimage.jpg" />
                </Head>
            </div>
        )

    }

}
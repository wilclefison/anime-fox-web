import React, { useEffect, useState } from 'react';
import Head from 'next/head'
import Player from '../player'
import NotFound from '../notfound'
import {
    EmailIcon,
    FacebookIcon,
    TelegramIcon,
    TwitterIcon,
    WhatsappIcon,
    EmailShareButton,
    FacebookShareButton,
    TelegramShareButton,
    TwitterShareButton,
    WhatsappShareButton,

} from "react-share";
import { AGrande } from '../Anuncios/anuncions'

export default function Movie(props) {
    const [content, setContent] = useState(props.data.res);
    const [isFetching, setIsFetching] = useState(false);
    const [play, setPlay] = useState(false);
    const [lEP, setLast] = useState(null);

    useEffect(() => {
        async function init() {
            try {
                if (props.data.episodio !== null) {
                    setPlay(true)
                }
                setLast(localStorage.getItem(`f/${content.url}`) != null ? JSON.parse(localStorage.getItem(`f/${content.url}`)) : null)
            } catch (e) {
                // console.log(e)
            } finally {
                setIsFetching(false);
            }
        }

        init();
    }, []);

    const selectMovie = (episodio) => {
        setPlay(true)
    }

    const handleChange = () => {
        setPlay(false)
    }

    if (content !== null) {

        return <div>
            {!play &&
                <section className="banner-wrapper overlay-wrapper" style={{ paddingTop: '35vw', backgroundImage: `url('${content.bg}')` }}>
                    <div className="banner-caption">
                        <div className="position-relative mb-4">
                            <a onClick={() => selectMovie(lEP)} className="d-flex align-items-center" style={{ cursor: 'pointer' }}>
                                <div className="play-button">
                                    <i className="fa fa-play" />
                                </div>
                                <h4 className="w-name text-white font-weight-700">{typeof window != 'undefined' ? localStorage.getItem(`f/${content.url}`) === null ? 'Assistir Agora' : 'Continuar Assistindo' : 'Assistir Agora'}</h4>
                            </a>
                        </div>
                        <ul className="list-inline p-0 m-0 share-icons music-play-lists">
                            <li><span><i className="ri-add-line" /></span></li>
                            <li><span><i className="ri-heart-fill" /></span></li>
                            <li className="share">
                                <span><i className="ri-share-fill" /></span>
                                <div className="share-box">
                                    <div className="d-flex align-items-center">
                                        <FacebookShareButton title={`${content.nome} - Assista no Anime Fox`} url={`http://animefox.me/filmes/${content.url}`} children={<FacebookIcon size={32} round={true} />} />
                                        <TwitterShareButton title={`${content.nome} - Assista no Anime Fox`} url={`http://animefox.me/filmes/${content.url}`} children={<TwitterIcon size={32} round={true} />} />
                                        <TelegramShareButton title={`${content.nome} - Assista no Anime Fox`} url={`http://animefox.me/filmes/${content.url}`} children={<TelegramIcon size={32} round={true} />} />
                                        <WhatsappShareButton title={`${content.nome} - Assista no Anime Fox`} url={`http://animefox.me/filmes/${content.url}`} children={<WhatsappIcon size={32} round={true} />} />
                                        <EmailShareButton title={`${content.nome} - Assista no Anime Fox`} url={`http://animefox.me/filmes/${content.url}`} children={<EmailIcon size={32} round={true} />} />
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
            }
            <div className="main-content">
                {!play && <div>
                    <section className="movie-detail container-fluid">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="trending-info g-border">
                                    <h1 className="trending-text big-title text-uppercase mt-0">{content.nome}</h1>
                                    <ul className="p-0 list-inline d-flex align-items-center movie-content">
                                        {content.categoria !== undefined && content.categoria.map((categoria, index) => <li key={index} className="text-white">{categoria.categoria_nome}</li>)}
                                    </ul>
                                    <div className="d-flex align-items-center text-white text-detail">
                                        <span >{content.dura}</span>
                                        <span className="trending-year">{content.data}</span>
                                    </div>
                                    <p className="trending-dec w-100 mb-0" dangerouslySetInnerHTML={{ __html: content.descricao }}></p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div className="container-fluid">
                            <div className="row">
                                <div className="col-sm-12 overflow-hidden pt-5">
                                    <div className="iq-main-header d-flex align-items-center justify-content-between">
                                        <h4 className="main-title">Sugestões</h4>
                                    </div>
                                </div>
                                <span className="animList">
                                    {
                                        content.extra.map((conteudo, index) => (
                                            <div className="list-item boxart-size-16x9 " key={index}>
                                                <div className="content content-vertical">
                                                    <div className="poster poster-vertical">
                                                        <a href={`/filmes/${conteudo.url}`} >
                                                            <div className="poster-text">{conteudo.nome}</div>
                                                            <div className="poster-image poster-shadow boxart-rounded" style={{
                                                                backgroundImage: `url(${conteudo.img})`,
                                                                backgroundPosition: 'center',
                                                                backgroundSize: 'cover',
                                                                backgroundRepeat: 'no-repeat'
                                                            }}><span className="poster-desc">
                                                                    {conteudo.nome}
                                                                </span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        ))
                                    }

                                </span>
                            </div>
                        </div>
                </div>}
                {play && <section className="playerAnime">
                    <Player episodio={content} animeNome={content.nome} animeId={content.id} url={content.url} onBack={handleChange} filme={true} />
                </section>}
                <AGrande id={1} />
            </div>
            <Head>
                <title>{`${content.nome} - Assista no Anime Fox`}</title>
                <meta property="og:url" content={`http://animefox.me/filmes/${content.url}`} />
                <meta property="og:type" content="article" />
                <meta property="og:title" content={`${content.nome} - Assista no Anime Fox`} />
                <meta property="og:description" content={`${content.descricao}`} />
                <meta property="og:image" content={`${content.bg}`} />

                <meta property="og:site_name" content={`${content.nome} - Assista no Anime Fox`} />
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:description" content={`${content.descricao}`} />
                <meta name="twitter:title" content={`${content.nome} - Assista no Anime Fox`} />
            </Head>
        </div>
    } else {
        return <NotFound statusCode={404} />

    }

}
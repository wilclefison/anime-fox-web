import React, { Component } from 'react'
import Head from 'next/head'

export default class NotFound extends Component {
    render() {
        const { statusCode } = this.props

        return (
            <div id="notfound">
                <div className="notfound">
                    <div className="notfound-404">
                        {statusCode == 404 ? <h1>4<span>0</span>4</h1> : <h1>O<span>P</span>S</h1>}
                    </div>
                    {statusCode == 404 ? <p>A pagina que você procura não foi encontrada.</p> : <p>Tivemos algum problema... Tente novamente mais tarde.</p>}
                </div>
                <img src={'/images/404.png'} className="imgbg" />
                <Head>
                    <title>Anime Fox - Assista a Naruto Shippuden, Bleach e episódios de outros animes online e de graça</title>
                    <meta property="og:locale" content="pt_BR" />
                    <meta property="og:type" content="article" />
                    <meta property="og:title" content="Anime Fox" />
                    <meta property="og:description" content="Assista Naruto Shippuden, Attack on Titan, Dragon Ball Super, e muito mais! Acompanhe suas séries favoritas, com legendas profissionais em português. Anime. Novos Episódios do Japão. Mais de 1000 Séries. Legendas em Português." />
                    <meta property="og:url" content="http://animefox.me/" />
                    <meta property="og:image" content="http://animefox.me/images/ogimage.jpg" />
                    <meta property="og:site_name" content="Anime Fox" />
                    <meta name="twitter:card" content="summary" />
                    <meta name="twitter:description" content="Assista Naruto Shippuden, Attack on Titan, Dragon Ball Super, e muito mais! Acompanhe suas séries favoritas, com legendas profissionais em português. Anime. Novos Episódios do Japão. Mais de 1000 Séries. Legendas em Português." />
                    <meta name="twitter:title" content="Anime Fox" />
                </Head>
            </div>
        )
    }
}
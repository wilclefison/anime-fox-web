import React from 'react'
import Header from './header'
import Footer from './footer'
import Head from 'next/head'
import { initGA, logPageView } from '../utils/analytics'

export default class Layout extends React.Component {
    constructor() {
        super();
        this.state = {
            loading: true
        };
    }

    componentDidMount() {
        if (!window.GA_INITIALIZED) {
            initGA()
            window.GA_INITIALIZED = true
        }
        logPageView()
        setTimeout(() => {
            this.setState({
                loading: false
            });
        }, 2000);
    }
    render() {
        const { loading } = this.state
        return (
            <div>
                <Head>
                    <meta charSet="utf-8" />
                    <meta name="keywords" content="anime, animes orion, boruto, dragon ball, animes online, nanatsu no taizai, dragon ball z, super animes, boku no hero, one punch man" />
                    <meta name="description" content="Anime Fox - Animes Online, A melhor maneira de assistir animes online grátis, basta dar play e ver seus animes favoritos em hd, atualizados diariamente." />
                    <link rel="canonical" href="http://animefox.me/" />
                    <link rel='dns-prefetch' href='//www.google.com' />
                    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png" />
                    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png" />
                    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png" />
                    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png" />
                    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png" />
                    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png" />
                    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png" />
                    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png" />
                    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png" />
                    <link rel="icon" type="image/png" sizes="192x192" href="/fav/android-icon-192x192.png" />
                    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png" />
                    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png" />
                    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png" />
                    <meta name="msapplication-TileColor" content="#000" />
                    <meta name="msapplication-TileImage" content="/fav/ms-icon-144x144.png" />
                    <meta name="theme-color" content="#000" />
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta name="theme-color" content="#000000" />
                    <link rel="shortcut icon" href="/images/favicon.ico" />
                    <link rel="stylesheet" href="/css/bootstrap.min.css" />
                    <link rel="stylesheet" href="/css/typography.css" />
                    <link rel="stylesheet" href="/css/style.css" />
                    <link rel="stylesheet" href="/css/responsive.css" />
                    <link rel="manifest" href="/manifest.json" />
                    <script id="_wauuup">var _wau = _wau || []; _wau.push(["small", "hhizmsxqc1", "uup"]);</script>
                    <script async src="//waust.at/s.js"></script>
                </Head>
                {loading && (
                    <div id="loading">
                        <div id="loading-center"></div>
                    </div>)
                }
                <Header />
                {this.props.children}
                <Footer />
                <div id="back-to-top">
                    <a className="top" href="#top" id="top"> <i className="fa fa-angle-up"></i> </a>
                </div>
            </div>
        )
    }
}

import React from 'react'
import ReactPlayer from '../reactplayer';
import { logEvent } from '../../utils/analytics'

export default class Player extends React.Component {
   intervalID = 0;
   state = {
      episodio: {},
      reproducao: [],
      animeNome: '',
      qualidade: [],
      url: null,
      nextEp: null,

   }
   componentDidMount() {
      const { episodio, filme } = this.props
      if (typeof (episodio) !== "undefined") {
         if (typeof (filme) !== "undefined") {
            this.loadMvData()
         } else {
            this.loadEpData()
         }
      } else {
         this.goBack()
      }

   }

   loadEpData = () => {
      const { episodio, animeNome, animeId } = this.props

      fetch(`${process.env.urlApi}api/player/episodios/${animeId}`)
         .then(res => res.json())
         .then((data) => {
            for (let i = 0; i < data.length; i++) {
               if (data[i].id == episodio.id) {
                  data[i].playing = true
               } else {
                  data[i].playing = false
               }
            }
            this.setState({ reproducao: data })
         })
      fetch(`${process.env.urlApi}api/link/${episodio.id}`)
         .then(res => res.json())
         .then((data) => {
            this.setState({ nextEp: data.nextEp, episodio, animeNome, animeId, qualidade: data.links, url: data.links[0].url })
         })
      this.intervalID = setInterval(() => { logEvent('Assitindo', `Anime: ${animeNome} - EP: ${this.state.episodio.numero} - Temp:  ${this.state.episodio.temporada}`) }, 60000)

   }

   loadMvData = () => {
      const { episodio, animeNome, animeId } = this.props

      fetch(`${process.env.urlApi}api/link-movie/${animeId}`)
         .then(res => res.json())
         .then((data) => {
            this.setState({ episodio, animeNome, animeId, qualidade: data.links, url: data.links[0].url })
         })
      this.intervalID = setInterval(() => { logEvent('Assitindo', `Anime: ${animeNome} - EP: ${this.state.episodio.numero} - Temp:  ${this.state.episodio.temporada}`) }, 60000)
   }

   goBack = () => {
      const { episodio } = this.state
      this.setState({ url: null })
      localStorage.setItem(`${this.props.url}`, JSON.stringify(episodio));
      this.props.onBack()
   }

   changeQualidade = (id) => {
      var { qualidade } = this.state;
      var url = null
      for (let i = 0; i < qualidade.length; i++) {
         if (qualidade[i].id === id) {
            qualidade[i].playing = true;
            url = qualidade[i].url;
         } else {
            qualidade[i].playing = false;
         }
      }
      this.setState({ url, qualidade })
   }

   onError = () => {
      const { animeNome, animeId } = this.props

      this.setState({ erroTrue: true, showControls: false })
      fetch(`${process.env.urlApi}api/player/error`, {
         method: "POST",
         body: JSON.stringify({ tipo: 1, msg: `{anime:${animeId}, anime_nome: '${animeNome}', episodio:${this.state.episodio.id} }` })
      });

   }

   proximoEpisodio = async (id, playing) => {
      if (!playing && id != undefined) {
         var { reproducao } = this.state;
         fetch(`${process.env.urlApi}api/ep/${id}`)
            .then(res => res.json())
            .then((data) => {
               localStorage.setItem(`${this.props.url}`, JSON.stringify(data[0]));
               for (let i = 0; i < reproducao.length; i++) {
                  if (reproducao[i].id == id) {
                     reproducao[i].playing = true
                  } else {
                     reproducao[i].playing = false
                  }
               }
               this.setState({ episodio: data[0], reproducao: reproducao })
            }).catch(() => {
               this.onError();
            })
         fetch(`${process.env.urlApi}api/link/${id}`)
            .then(res => res.json())
            .then((data) => {
               this.setState({ nextEp: data.nextEp, qualidade: data.links, url: data.links[0].url })
            }).catch(() => {
               this.onError();
            })
      }
   }
   render() {
      const { episodio, animeNome, qualidade, url, nextEp, reproducao } = this.state
      const { filme } = this.props
      var dataAnime = null
      if (filme) {
         dataAnime = {
            subTitle: `${episodio.data} `,
            extraInfoMedia: `${episodio.data} `,
         };
      } else {
         dataAnime = {
            subTitle: `T${episodio.temporada}:E${episodio.numero}  ${episodio.nome}`,
            extraInfoMedia: `T${episodio.temporada}:E${episodio.numero}  ${episodio.nome}`,
         };
      }

      return (
         <div>
            {
               url != null &&
               <ReactPlayer
                  // Vídeo Link - Just data is required
                  src={url}
                  title={animeNome}
                  subTitle={dataAnime.subTitle}
                  titleMedia={animeNome}
                  extraInfoMedia={dataAnime.extraInfoMedia}
                  extraDescMedia={episodio.descricao}
                  // Text language of player
                  playerLanguage="pt"
                  // Action when the button X (close) is clicked
                  backButton={() => { this.goBack() }}
                  onCrossClick={() => { this.goBack() }}
                  //Error Receptor 
                  onErrorVideo={() => { onError() }}
                  // The player use the all viewport
                  fullPlayer
                  autoPlay={true}
                  startPosition={0}
                  // The info of the next video action
                  dataNext={nextEp}
                  // The action call when the next video is clicked
                  onNextClick={(i) => { this.proximoEpisodio(i, false) }}
                  // The list reproduction data, will be render in this order
                  reprodutionList={reproducao}
                  //Qualidade
                  qualities={qualidade}
                  playbackRateEnable={false}
                  onChangeQuality={(i) => { this.changeQualidade(i) }}
                  // The function call when a item in reproductionList is clicked
                  onClickItemListReproduction={(id, playing) => { this.proximoEpisodio(id, playing) }}
                  // The function is call when the video finish
                  onEnded={() => { }}
                  // Enable the orverlay when player is paused
                  overlayEnabled
                  // Enabled the auto clode controlls of player
                  autoControllCloseEnabled
                  // Style
                  primaryColor="#ff9201"
                  secundaryColor="#ffffff"
                  fontFamily="-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
               />
            }

         </div>
      )

   }
}
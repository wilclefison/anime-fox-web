import React from 'react'
import Loader from 'react-loader-spinner'
import Head from 'next/head'
import { AGrande } from '../Anuncios/anuncions'


export default class Categorias extends React.Component {
    state = {
        categories: this.props.data.categories
    }

    render() {

        return (
            <div className="main-content">
                <section className="container-fluid">
                    <div className="now-container cat-now-con">
                        <div style={{ textAlign: 'center', padding: '10px 0px 0px 0px' }}>
                            <AGrande id={0} />
                        </div>
                        {this.state.categories.length === 0 &&
                            <div style={{ width: "100%", height: "100", display: "flex", justifyContent: "center", alignItems: "center" }} >
                                <Loader type="ThreeDots" color="#2BAD60" height={100} width={100} />
                            </div>
                        }
                        {
                            this.state.categories &&
                            <div className="list container-items">
                            <h2 className="main-title">Categorias</h2>
                            <span className="catList">
                                {
                                     this.state.categories.map((conteudo, index) => (
                                        <div className="list-item boxart-size-16x9 " key={index}>
                                            <div className="content content-vertical">
                                                <div className="poster poster-vertical">
                                                    <a href={`/categoria/${conteudo.categoria}`} >
                                                        <div className="poster-image poster-shadow boxart-rounded" style={{
                                                            backgroundImage: `url(/images/categoria/${conteudo.id}-min.jpg)`,
                                                            backgroundSize: 'cover',
                                                            backgroundRepeat: 'no-repeat'
                                                        }}><span className="poster-desc">
                                                                {conteudo.categoria}
                                                            </span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }

                            </span>
                        </div>
                        }
                    </div>
                </section>
                <div style={{ textAlign: 'center', padding: '5vh 0px' }}>
                    <AGrande id={1} />
                </div>
                <Head>
                    <title>Anime Fox - Categorias</title>
                    <meta property="og:url" content="http://animefox.me/" />
                    <meta property="og:type" content="article" />
                    <meta property="og:title" content="Anime Fox" />
                    <meta property="og:description" content="Assista Naruto Shippuden, Attack on Titan, Dragon Ball Super, e muito mais! Acompanhe suas séries favoritas, com legendas profissionais em português. Anime. Novos Episódios do Japão. Mais de 1000 Séries. Legendas em Português." />
                    <meta property="og:image" content="http://animefox.me/images/ogimage.jpg" />
                </Head>
            </div>
        )

    }

}
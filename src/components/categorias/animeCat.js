
import React from 'react'
import Loader from 'react-loader-spinner'
import Head from 'next/head'
import { AGrande } from '../Anuncios/anuncions'

export default class AnimeCat extends React.Component {
    state = {
        content: this.props.data.data.conteudo,
        page: 24,
        totalPages: this.props.data.data.contagem,
        loadingScroll: false
    }
    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }
    loadCategoria = async () => {
        const catId = this.props.data.pid;
        if (this.state.page <= this.state.totalPages) {
            fetch(`${process.env.urlApi}api/categoria/${catId}?start=${this.state.page}&limit=24`)
                .then(res => res.json())
                .then((data) => {
                    this.setState({ content: this.state.content.concat(data.conteudo), page: this.state.page + 24, loadingScroll: false })
                }).catch(console.log);
        }
    }
    handleScroll = () => {
        const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        const body = document.body;
        const html = document.documentElement;
        const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
        const windowBottom = windowHeight + window.pageYOffset + 80;

        if (windowBottom >= docHeight - 1) {
            if (this.state.page <= this.state.totalPages) {
                this.setState({ loadingScroll: true })
                this.loadCategoria()
            }

        }
    }
    render() {
        const { content, loadingScroll } = this.state
        return (
            <div className="main-content">
                <section className="container-fluid">
                    <div style={{ textAlign: 'center', padding: '10px 0px 0px 0px' }}>
                        <AGrande id={0} />
                    </div>
                    <div className="now-container cat-now-con">
                        {content.length === 0 &&
                            <div style={{ width: "100%", height: "100", display: "flex", justifyContent: "center", alignItems: "center" }} >
                                <Loader type="ThreeDots" color="#2BAD60" height={100} width={100} />
                            </div>
                        }
                        {(content.length !== 0) &&
                            <div className="list container-items">
                                <h2 className="main-title">{this.props.data.pid}</h2>
                                <span className="animList">
                                    {
                                        content.map((conteudo, index) => (
                                            <div className="list-item boxart-size-16x9 " key={index}>
                                                <div className="content content-vertical">
                                                    <div className="poster poster-vertical">
                                                        <a href={`/anime/${conteudo.url}`} >
                                                            <div className="poster-text">{conteudo.anime_nome}</div>
                                                            <div className="poster-image poster-shadow boxart-rounded" style={{
                                                                backgroundImage: `url(https://image.tmdb.org/t/p/w342/${conteudo.anime_imagem == null ? conteudo.anime_bg : conteudo.anime_imagem})`,
                                                                backgroundPosition: 'center',
                                                                backgroundSize: 'cover',
                                                                backgroundRepeat: 'no-repeat'
                                                            }}><span className="poster-desc">
                                                                    {conteudo.anime_nome}
                                                                </span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        ))
                                    }

                                </span>
                            </div>
                        }
                    </div>
                    {loadingScroll && <div style={{ width: "100%", height: "100", display: "flex", justifyContent: "center", alignItems: "center" }} >
                        <Loader type="ThreeDots" color="#2BAD60" height={100} width={100} />
                    </div>}
                </section>
                <div style={{ textAlign: 'center', padding: '5vh 0px' }}>
                    <AGrande id={1} />
                </div>
                <Head>
                    <title>{`Anime Fox - ${this.props.data.pid}`}</title>
                    <meta property="og:url" content="http://animefox.me/" />
                    <meta property="og:type" content="article" />
                    <meta property="og:title" content="Anime Fox" />
                    <meta property="og:description" content="Assista Naruto Shippuden, Attack on Titan, Dragon Ball Super, e muito mais! Acompanhe suas séries favoritas, com legendas profissionais em português. Anime. Novos Episódios do Japão. Mais de 1000 Séries. Legendas em Português." />
                    <meta property="og:image" content="http://animefox.me/images/ogimage.jpg" />
                </Head>
            </div>
        )

    }

}
import React, { useEffect, useState, useRef } from "react";
import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import {
  FaUndoAlt,
  FaPlay,
  FaPause,
  FaVolumeUp,
  FaVolumeDown,
  FaVolumeOff,
  FaVolumeMute,
  FaArrowLeft,
  FaExpand,
  FaStepForward,
  FaCog,
  FaClone,
  FaCompress,
  FaRedoAlt,
} from "react-icons/fa";
import { FiCheck, FiX } from "react-icons/fi";
import "./style.css";
import translations from "./translations";

i18n.use(initReactI18next).init({
  resources: translations,
  lng: "pt",
  fallbackLng: "pt",

  interpolation: {
    escapeValue: false,
  },
});

export default function ReactPlayer({
  title = false,
  subTitle = false,
  titleMedia = false,
  extraInfoMedia = false,
  extraDescMedia = false,
  playerLanguage = "pt",

  backButton = false,

  src,
  autoPlay = true,

  onCanPlay = false,
  onTimeUpdate = false,
  onEnded = false,
  onErrorVideo = false,
  onNextClick = false,
  onClickdiv = false,
  onCrossClick = () => {},
  startPosition = 0,

  dataNext = {},
  reprodutionList = [],
  qualities = [],
  onChangeQuality = [],
  playbackRateEnable = true,
  overlayEnabled = true,
  autoControllCloseEnabled = true,

  // subtitleMedia,
}) {
  // Referências
  const videoComponent = useRef(null);
  const timerRef = useRef(null);
  const timerBuffer = useRef(null);
  const playerElement = useRef(null);
  const listReproduction = useRef(null);

  // Estados
  const [videoReady, setVideoReady] = useState(false);
  const [playing, setPlaying] = useState(true);
  const [progress, setProgress] = useState(0);
  const [duration, setDuration] = useState(0);
  const [end, setEnd] = useState(false);
  const [controlBackEnd, setControlBackEnd] = useState(false);
  const [fullScreen, setFullScreen] = useState(false);
  const [volume, setVolume] = useState(100);
  const [muted, setMuted] = useState(false);
  const [error, setError] = useState(false);
  const [waitingBuffer, setWaitingBuffer] = useState(false);
  const [showControls, setShowControls] = useState(false);
  const [showInfo, setShowInfo] = useState(false);
  const [playbackRate, setPlaybackRate] = useState(1);
  const [started, setStarted] = useState(false);

  const [showControlVolume, setShowControlVolume] = useState(false);
  const [showQuality, setShowQuality] = useState(false);
  const [showDataNext, setShowDataNext] = useState(false);
  const [showPlaybackRate, setShowPlaybackRate] = useState(false);
  const [showReproductionList, setShowReproductionList] = useState(false);

  const playbackRateOptions = [
    "0.25",
    "0.5",
    "0.75",
    "Normal",
    "1.25",
    "1.5",
    "2",
  ];

  const { t } = useTranslation();

  const [, setActualBuffer] = useState({
    index: 0,
    start: 0,
    end: 0,
  });

  const secondsToHms = (d) => {
    d = Number(d);
    const h = Math.floor(d / 3600);
    const m = Math.floor((d % 3600) / 60);
    let s = Math.floor((d % 3600) % 60);

    if (s < 10) {
      s = `0${s}`;
    }

    if (h) {
      return `${h}:${m}:${s}`;
    }

    return `${m}:${s}`;
  };

  const timeUpdate = (e) => {
    setProgress(e.target.currentTime);
  };

  const goToPosition = (position) => {
    videoComponent.current.currentTime = position;
    setProgress(position);
  };

  const play = () => {
    setPlaying(!playing);

    if (videoComponent.current.paused) {
      videoComponent.current.play();
      return;
    }

    videoComponent.current.pause();
  };

  const onEndedFunction = () => {
    if (
      +startPosition === +videoComponent.current.duration &&
      !controlBackEnd
    ) {
      setControlBackEnd(true);
      videoComponent.current.currentTime = videoComponent.current.duration - 30;
      if (autoPlay) {
        setPlaying(true);
        videoComponent.current.play();
      } else {
        setPlaying(false);
      }
    } else {
      setEnd(true);
      setPlaying(false);

      if (onEnded) {
        onEnded();
      }
    }
  };

  const nextSeconds = (seconds) => {
    const current = videoComponent.current.currentTime;
    const total = videoComponent.current.duration;

    if (current + seconds >= total - 2) {
      videoComponent.current.currentTime = videoComponent.current.duration - 1;
      setProgress(videoComponent.current.duration - 1);
      return;
    }

    videoComponent.current.currentTime += seconds;
    setProgress(videoComponent.current.currentTime + seconds);
  };

  const previousSeconds = (seconds) => {
    const current = videoComponent.current.currentTime;

    if (current - seconds <= 0) {
      videoComponent.current.currentTime = 0;
      setProgress(0);
      return;
    }

    videoComponent.current.currentTime -= seconds;
    setProgress(videoComponent.current.currentTime - seconds);
  };

  const startVideo = () => {
    try {
      setDuration(videoComponent.current.duration);
      setVideoReady(true);

      if (!started) {
        setStarted(true);
        setPlaying(false);

        if (autoPlay) {
          videoComponent.current.play();
          setPlaying(!videoComponent.current.paused);
        }
      }

      if (onCanPlay) {
        onCanPlay();
      }
    } catch (err) {
      setPlaying(false);
    }
  };

  const erroVideo = () => {
    if (onErrorVideo) {
      onErrorVideo();
    }
    setError(t("playError", { lng: playerLanguage }));
  };

  const setMuttedAction = (value) => {
    setMuted(value);
    setShowControlVolume(false);
    videoComponent.current.muted = value;
  };

  const setVolumeAction = (value) => {
    setVolume(value);
    videoComponent.current.volume = value / 100;
  };

  const exitFullScreen = () => {
    if (
      document.webkitIsFullScreen ||
      document.mozFullScreen ||
      document.msFullscreenElement ||
      document.fullscreenElement
    ) {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else {
        document.webkitExitFullscreen();
      }

      setFullScreen(false);
    }
  };

  const enterFullScreen = () => {
    setShowInfo(true);
    if (playerElement.current.requestFullscreen) {
      playerElement.current.requestFullscreen();
      setFullScreen(true);
    } else if (playerElement.current.webkitRequestFullscreen) {
      playerElement.current.webkitRequestFullscreen();
      setFullScreen(true);
    }
  };

  const chooseFullScreen = () => {
    if (
      document.webkitIsFullScreen ||
      document.mozFullScreen ||
      document.msFullscreenElement ||
      document.fullscreenElement
    ) {
      document.exitFullscreen();
      return;
    }

    setShowInfo(true);

    if (playerElement.current.requestFullscreen) {
      playerElement.current.requestFullscreen();
    } else if (playerElement.current.webkitRequestFullscreen) {
      playerElement.current.webkitRequestFullscreen();
    } else if (playerElement.current.mozRequestFullScreen) {
      playerElement.current.mozRequestFullScreen();
    } else if (playerElement.current.msRequestFullscreen) {
      playerElement.current.msRequestFullscreen();
    }

    setFullScreen(true);
  };

  const setStateFullScreen = () => {
    if (
      !document.webkitIsFullScreen &&
      !document.mozFullScreen &&
      !document.msFullscreenElement &&
      !document.fullscreenElement
    ) {
      setFullScreen(false);
      return;
    }

    setFullScreen(true);
  };

  const controllScreenTimeOut = () => {
    if (!autoControllCloseEnabled) {
      setShowInfo(true);
      return;
    }

    setShowControls(false);
    if (!playing) {
      setShowInfo(true);
    }
  };

  const hoverScreen = () => {
    setShowControls(true);
    setShowInfo(false);

    if (timerRef.current) {
      clearTimeout(timerRef.current);
    }

    timerRef.current = setTimeout(controllScreenTimeOut, 2000);
  };

  const getKeyBoardInteration = (e) => {
    if (e.keyCode === 32 && videoComponent.current) {
      if (videoComponent.current.paused) {
        videoComponent.current.play();
        setPlaying(true);
        hoverScreen();
      } else {
        videoComponent.current.pause();
        setPlaying(false);
        hoverScreen();
      }
    }
  };

  const scrollToSelected = () => {
    const element = listReproduction.current;
    const selected = element.getElementsByClassName("selected")[0];
    const position = selected.offsetTop;
    const height = selected.offsetHeight;
    element.scrollTop = position - height * 2;
  };

  const onChangePlayBackRate = (speed) => {
    speed = speed === "Normal" ? 1 : speed;
    videoComponent.current.playbackRate = speed;
    setPlaybackRate(speed);
  };

  useEffect(() => {
    let mounted = true;
    if (showReproductionList && mounted) {
      scrollToSelected();
    }
    return () => (mounted = false);
  }, [showReproductionList]);

  useEffect(() => {
    let mounted = true;

    if (src && mounted) {
      videoComponent.current.currentTime = startPosition;
      setProgress(0);
      setDuration(0);
      setVideoReady(false);
      setError(false);
      setShowReproductionList(false);
      setShowDataNext(false);
      setActualBuffer({
        index: 0,
        start: 0,
        end: 0,
      });
      setStarted(false);
      setPlaying(true);
    }
    return () => (mounted = false);
  }, [src]);

  useEffect(() => {
    document.addEventListener("keydown", getKeyBoardInteration, false);
    playerElement.current.addEventListener(
      "fullscreenchange",
      setStateFullScreen,
      false
    );
  }, []);

  // When changes happen in fullscreen document, teh state of fullscreen is changed
  useEffect(() => {
    setStateFullScreen();
  }, [
    document.fullscreenElement,
    document.webkitIsFullScreen,
    document.mozFullScreen,
    document.msFullscreenElement,
  ]);

  function renderLoading() {
    return (
      <div className="loading">
        <div>
          <div />
          <div />
          <div />
        </div>
      </div>
    );
  }

  function renderInfoVideo() {
    return (
      <div
        className="standybyinfo"
        style={{
          opacity:
            showInfo === true && videoReady === true && playing === false
              ? 1
              : 0,
        }}
      >
        {(title || subTitle) && (
          <section className="center">
            <h3 className="text">
              {t("youAreWatching", { lng: playerLanguage })}
            </h3>
            <h1 className="title">{title}</h1>
            <h2 className="sub-title">{subTitle}</h2>
            <h4 className="text" style={{ width: "50%", marginTop: 10 }}>
              {extraDescMedia}
            </h4>
          </section>
        )}
        <footer>{t("paused", { lng: playerLanguage })}</footer>
      </div>
    );
  }

  function renderCloseVideo() {
    return (
      <div
        className="videopreloading"
        style={{
          opacity:
            videoReady === false || (videoReady === true && error) ? 1 : 0,
          zIndex:
            videoReady === false || (videoReady === true && error) ? 2 : 0,
        }}
      >
        {(title || subTitle) && (
          <header>
            <div>
              <h1>{title}</h1>
              <h2>{subTitle}</h2>
            </div>
            <FiX onClick={onCrossClick} />
          </header>
        )}

        <section style={{ opacity: !!error ? 1 : 0 }}>
          {error && (
            <div>
              <h1>{error}</h1>
              {qualities.length > 1 && (
                <div>
                  <p>
                    {t("tryAccessingOtherQuality", { lng: playerLanguage })}
                  </p>
                  <div className="links-error">
                    {qualities.map((item) => (
                      <div onClick={() => onChangeQuality(item.id)}>
                        {item.prefix && <span>HD</span>}
                        <span>{item.nome}</span>
                        {item.playing && <FiX />}
                      </div>
                    ))}
                  </div>
                </div>
              )}
            </div>
          )}
        </section>
      </div>
    );
  }

  return (
    <div
      className="containerVid"
      onMouseMove={hoverScreen}
      ref={playerElement}
      onDoubleClick={chooseFullScreen}
    >
      {(videoReady === false || (waitingBuffer === true && playing === true)) &&
        !error &&
        !end &&
        renderLoading()}

      {!!overlayEnabled && renderInfoVideo()}

      {renderCloseVideo()}

      {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
      <video
        ref={videoComponent}
        src={src}
        controls={false}
        onCanPlay={() => startVideo()}
        onTimeUpdate={timeUpdate}
        onError={erroVideo}
        onEnded={onEndedFunction}
        style={{ opacity: !!error ? 0 : 1 }}
      >
        {/* <track label="English" kind="subtitles" srcLang="en" src={subtitleMedia} default /> */}
      </video>

      <div
        className="controleVid"
        style={{
          opacity:
            showControls === true && videoReady === true && error === false
              ? 1
              : 0,
          transform:
            showControls === true && videoReady === true && error === false
              ? "scale(1)"
              : "scale(1.2)",
        }}
      >
        {backButton && (
          <div className="back">
            <div onClick={backButton} style={{ cursor: "pointer" }}>
              <FaArrowLeft />
            </div>
          </div>
        )}

        {showControlVolume !== true &&
          showQuality !== true &&
          !showDataNext &&
          !showReproductionList && (
            <div className="line-reproduction">
              <input
                type="range"
                value={progress}
                className="progress-bar"
                max={duration}
                onChange={(e) => goToPosition(e.target.value)}
                title=""
                style={{
                  background: `linear-gradient(
                    93deg,
                    #ff9201 ${(progress * 100) / duration}%,
                    #fff ${(progress * 100) / duration}%
                  )`,
                }}
              />
              <span>{secondsToHms(duration - progress)}</span>
            </div>
          )}

        {videoReady === true && (
          <div className="controlls">
            <div className="start">
              <div className="item-control">
                {!playing && <FaPlay onClick={play} />}
                {playing && <FaPause onClick={play} />}
              </div>

              <div className="item-control">
                <FaUndoAlt onClick={() => previousSeconds(5)} />
              </div>

              <div className="item-control">
                <FaRedoAlt onClick={() => nextSeconds(5)} />
              </div>

              {muted === false && (
                <div
                  onMouseLeave={() => setShowControlVolume(false)}
                  className="item-control"
                >
                  {showControlVolume === true && (
                    <div className="volumn-controll">
                      <div className="box-connector" />
                      <div className="box">
                        <input
                          type="range"
                          min={0}
                          max={100}
                          value={volume}
                          onChange={(e) => setVolumeAction(e.target.value)}
                          title=""
                          style={{
                            background: `linear-gradient(
                            93deg,
                            #ff9201 ${volume}%,
                            #fff ${volume}%)`,
                          }}
                        />
                      </div>
                    </div>
                  )}

                  {volume >= 60 && (
                    <FaVolumeUp
                      onMouseEnter={() => setShowControlVolume(true)}
                      onClick={() => setMuttedAction(true)}
                    />
                  )}

                  {volume < 60 && volume >= 10 && (
                    <FaVolumeDown
                      onMouseEnter={() => setShowControlVolume(true)}
                      onClick={() => setMuttedAction(true)}
                    />
                  )}

                  {volume < 10 && volume > 0 && (
                    <FaVolumeOff
                      onMouseEnter={() => setShowControlVolume(true)}
                      onClick={() => setMuttedAction(true)}
                    />
                  )}

                  {volume <= 0 && (
                    <FaVolumeMute
                      onMouseEnter={() => setShowControlVolume(true)}
                      onClick={() => setVolumeAction(0)}
                    />
                  )}
                </div>
              )}

              {muted === true && (
                <div className="item-control">
                  <FaVolumeMute onClick={() => setMuttedAction(false)} />
                </div>
              )}

              <div className="item-control info-video">
                <span className="info-first">{titleMedia}</span>
                <span className="info-secund">{extraInfoMedia}</span>
              </div>
            </div>

            <div className="end">
              <div className="item-control">
                <a href={"https://www.cvv.org.br/"} target={"_blank"}>
                  <img
                    src="https://i0.wp.com/www.cvv.org.br/wp-content/themes/cvv/assets/images/logo.png"
                    className="imgCvv"
                  />
                </a>
              </div>
              {!!playbackRateEnable && (
                <div
                  className="item-control"
                  onMouseLeave={() => setShowPlaybackRate(false)}
                >
                  {showPlaybackRate === true && (
                    <div className="itemplaybackrate">
                      <div>
                        <div className="title">
                          {t("speeds", { lng: playerLanguage })}
                        </div>
                        {playbackRateOptions.map((item) => (
                          <div
                            className="item"
                            onClick={() => onChangePlayBackRate(item)}
                          >
                            {(+item === +playbackRate ||
                              (item === "Normal" && +playbackRate === 1)) &&
                              FiCheck()}
                            <div className="bold">
                              {item === "Normal" ? item : `${item}x`}
                            </div>
                          </div>
                        ))}
                      </div>
                      <div className="box-connector" />
                    </div>
                  )}

                  <div
                    className="item-controlBar playbackRate iconplaybackrate"
                    onMouseEnter={() => setShowPlaybackRate(true)}
                  >
                    <span>
                      {playbackRate === "Normal" ? "1" : `${playbackRate}`}
                      <small>x</small>
                    </span>
                  </div>
                </div>
              )}

              {onNextClick && (
                <div
                  className="item-control"
                  onMouseLeave={() => setShowDataNext(false)}
                >
                  {showDataNext === true && dataNext.title && (
                    <div className="item-controlBar itemnext">
                      <div>
                        <div className="title">
                          {t("nextEpisode", { lng: playerLanguage })}
                        </div>
                        <div
                          className="item"
                          onClick={() => {
                            onNextClick(dataNext.id);
                          }}
                        >
                          {dataNext.img && (
                            <img
                              className="cover"
                              src={dataNext.img}
                              alt={dataNext.title}
                            />
                          )}
                          <div className="columFlex">
                            <div className="bold btm">{dataNext.title}</div>
                            {dataNext.description && (
                              <div>{dataNext.description}</div>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="box-connector" />
                    </div>
                  )}

                  <FaStepForward
                    onClick={() => {
                      onNextClick(dataNext.id);
                    }}
                    onMouseEnter={() => setShowDataNext(true)}
                  />
                </div>
              )}

              <div
                className="item-control"
                onMouseLeave={() => setShowReproductionList(false)}
              >
                {showReproductionList && (
                  <div className="item-controlBar itemlistReprodution">
                    <div>
                      <div className="title">
                        {t("playlist", { lng: playerLanguage })}
                      </div>
                      <div
                        ref={listReproduction}
                        className="list-list-reproduction scroll-clean-player"
                      >
                        {reprodutionList.map((item, index) => (
                          <div
                            key={index}
                            className={`item-list-reproduction ${
                              item.playing && "selected"
                            }`}
                            onClick={() =>
                              onClickdiv && onClickdiv(item.id, item.playing)
                            }
                          >
                            <div className="bold">
                              <span
                                style={{ marginRight: 15 }}
                              >{`T:${item.temporada} E:${item.numero}`}</span>
                              {item.nome}
                            </div>

                            {item.percent && <div className="percent" />}
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="box-connector" />
                  </div>
                )}
                {reprodutionList && reprodutionList.length > 1 && (
                  <FaClone onMouseEnter={() => setShowReproductionList(true)} />
                )}
              </div>

              {qualities && qualities.length > 1 && (
                <div
                  className="item-control"
                  onMouseLeave={() => setShowQuality(false)}
                >
                  {showQuality === true && (
                    <div className="item-controlBar itemlistQualiti">
                      <div>
                        {qualities &&
                          qualities.map((item) => (
                            <div
                              key={item.id}
                              onClick={() => {
                                setShowQuality(false);
                                onChangeQuality(item.id);
                              }}
                            >
                              {item.prefix && <span>HD</span>}

                              <span>{item.nome}</span>
                              {item.playing && <FiCheck />}
                            </div>
                          ))}
                      </div>
                      <div className="box-connector" />
                    </div>
                  )}

                  <FaCog onMouseEnter={() => setShowQuality(true)} />
                </div>
              )}

              <div className="item-control">
                {fullScreen === false && <FaExpand onClick={enterFullScreen} />}
                {fullScreen === true && <FaCompress onClick={exitFullScreen} />}
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

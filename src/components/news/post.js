import React from 'react'
import Head from 'next/head'
import { DiscussionEmbed } from 'disqus-react';
import { EmailIcon, FacebookIcon, TelegramIcon, TwitterIcon, WhatsappIcon, EmailShareButton, FacebookShareButton, TelegramShareButton, TwitterShareButton, WhatsappShareButton } from "react-share";
import Api from "../../services/Api";
import NotFound from '../notfound'
import * as Anuncio from '../Anuncios/anuncions'


export default class Post extends React.Component {
    state = {
        data: this.props.data,
        noContent: true
    }

    render() {
        const { data, noContent } = this.state;
        if (data != null) {
            const d = new Date(data.dia)
            const dtf = new Intl.DateTimeFormat('pt-br', { year: 'numeric', month: 'short', day: '2-digit' })
            const [{ value: da }, , { value: mo }, , { value: ye }] = dtf.formatToParts(d)

            return (
                <div className="main-content">
                    <section className="container-fluid">
                        <div className="content">
                            <div className="row mt-5">
                                <div className="col-xl-8  mt-5">
                                    <div className="post_details">
                                        <h1 className="post_page_title">{data.titulo}</h1>
                                        <span className="post_page_data">{`${da} ${mo} ${ye}`}</span>
                                        <div className="post_preview post_preview_page">
                                            <picture>
                                                <img src={data.img} alt={data.titulo} loading="lazy" />
                                            </picture>
                                        </div>
                                        <div style={{ textAlign: 'center', padding: '1vh 0px' }} className="AnProps">
                                            <Anuncio.AGrande id={0} />
                                        </div>
                                        <div className="post_des_wrapper">
                                            <div className="post_des" dangerouslySetInnerHTML={{ __html: data.conteudo }} />
                                        </div>
                                    </div>
                                    <div className="post_page_comments">
                                        <div className="post_tags">
                                            {data.tags.map(x => (
                                                <a href="/tags/javascript/">#{x.Tag_Title}</a>
                                            ))}
                                        </div>
                                        <div className="post_share_content"><span>Compartilhar:</span>
                                            <FacebookShareButton title={`${data.titulo} - Anime Fox`} url={`http://animefox.me/post/${data.link}`} children={<FacebookIcon size={32} round={true} />} />
                                            <TwitterShareButton title={`${data.titulo} - Anime Fox`} url={`http://animefox.me/post/${data.link}`} children={<TwitterIcon size={32} round={true} />} />
                                            <TelegramShareButton title={`${data.titulo} - Anime Fox`} url={`http://animefox.me/post/${data.link}`} children={<TelegramIcon size={32} round={true} />} />
                                            <WhatsappShareButton title={`${data.titulo} - Anime Fox`} url={`http://animefox.me/post/${data.link}`} children={<WhatsappIcon size={32} round={true} />} />
                                            <EmailShareButton title={`${data.titulo} - Anime Fox`} url={`http://animefox.me/post/${data.link}`} children={<EmailIcon size={32} round={true} />} />
                                        </div>
                                    </div>
                                    <DiscussionEmbed
                                        shortname='animefox-1'
                                        config={
                                            {
                                                url: `http://animefox.me/post/${data.link}`,
                                                identifier: data.link,
                                                title: data.titulo,
                                                language: 'pt_BR'
                                            }
                                        }
                                    />
                                </div>
                                <div className="col-xl-4  mt-5">
                                    <div className="post_division_content">
                                        <h2 className="post_lateral_title">Promotion</h2>
                                        <div className="text-center mt-4">
                                            <Anuncio.Amazon3 />
                                        </div>
                                    </div>
                                    <div className="post_division_content mt-4">
                                        <h2 className="post_lateral_title">Ultimos Posts</h2>
                                        <div className="d-flex flex-column  align-items-center text-center  mt-4">
                                            {data.ultimos.map(noticia => (
                                                <div className="col-sm-10 post-item" key={noticia.id}>
                                                    <article className="post bb-post bb-card-item post-33042 type-post status-publish format-standard has-post-thumbnail category-indicacoes reaction-amei reaction-kimochi">
                                                        <div className="post-thumbnail">
                                                            <div className="bb-badge-list">
                                                                <a className="bb-badge badge category category-25" href="https://otakubfx.com.br/categoria/indicacoes/" title="Indicações"><span className="circle">
                                                                    <img src="https://i0.wp.com/otakubfx.com.br/wp-content/uploads/2020/03/indicacao-2.png?resize=150%2C150&ssl=1" alt="Indicações" />
                                                                </span>
                                                                    <span className="text">Indicações</span></a>
                                                            </div>
                                                            <a href={`/post/${noticia.link}`} title={noticia.titulo}> <div className="bb-media-placeholder" style={{ paddingBottom: '75%' }}>
                                                                <img src={noticia.img} alt={noticia.titulo} className="attachment-boombox_image360x270 size-boombox_image360x270 wp-post-image imgpost lateral" />
                                                            </div>
                                                            </a>
                                                        </div>
                                                        <div className="content">
                                                            <header className="entry-header">
                                                                <h2 className="entry-title">
                                                                    <a href={`/post/${noticia.link}`} rel="bookmark">{noticia.titulo}</a>
                                                                </h2>
                                                            </header>
                                                        </div>
                                                    </article>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                    <div className="post_division_content">
                                        <h2 className="post_lateral_title">Promotion</h2>
                                        <div className="text-center mt-4">
                                            <Anuncio.Amazon3 />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="fnEApR mt-4">
                                <h2 className="ljHpYd">Mais Posts</h2>
                                <div className="row mt-4">
                                    {data.mais.map(noticia => (
                                        <div className="col-sm-2 post-item" key={noticia.id}>
                                            <article className="post bb-post bb-card-item post-33042 type-post status-publish format-standard has-post-thumbnail category-indicacoes reaction-amei reaction-kimochi">
                                                <div className="post-thumbnail">
                                                    <div className="bb-badge-list">
                                                        <a className="bb-badge badge category category-25" href="https://otakubfx.com.br/categoria/indicacoes/" title="Indicações"><span className="circle">
                                                            <img src="https://i0.wp.com/otakubfx.com.br/wp-content/uploads/2020/03/indicacao-2.png?resize=150%2C150&ssl=1" alt="Indicações" />
                                                        </span>
                                                            <span className="text">Indicações</span></a>
                                                    </div>
                                                    <a href={`/post/${noticia.link}`} title={noticia.titulo}> <div className="bb-media-placeholder" style={{ paddingBottom: '75%' }}>
                                                        <img src={noticia.img} alt={noticia.titulo} className="attachment-boombox_image360x270 size-boombox_image360x270 wp-post-image imgpost" />
                                                    </div>
                                                    </a>
                                                </div>
                                                <div className="content">
                                                    <header className="entry-header">
                                                        <h2 className="entry-title">
                                                            <a href={`/post/${noticia.link}`} rel="bookmark">{noticia.titulo}</a>
                                                        </h2>
                                                    </header>
                                                </div>
                                            </article>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </section>
                    <Head>
                        <title>{data.titulo} - Anime Fox</title>
                        <meta property="og:url" content={`http://animefox.me/post/${data.link}`} />
                        <meta property="og:type" content="article" />
                        <meta property="og:title" content={`${data.titulo} - Anime Fox`} />
                        <meta property="og:image" content={data.img} />
                        <meta property="og:description" content={data.descricao} />

                        <meta property="og:site_name" content={`${data.titulo} - Anime Fox`} />
                        <meta name="twitter:card" content="summary" />
                        <meta name="twitter:title" content={`${data.titulo} - Anime Fox`} />
                    </Head>
                </div >
            )
        } else {
            return <NotFound statusCode={404} />

        }

    }

}
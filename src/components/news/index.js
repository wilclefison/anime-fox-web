import React from "react";
import Head from "next/head";
import Loader from "react-loader-spinner";
import Slider from "react-slick";
import * as Anuncio from "../Anuncios/anuncions";

const convertDays = (data, type) => {
  data = new Date(data);
  var dtf = new Intl.DateTimeFormat("pt", { month: "short", day: "2-digit" });
  var [{ value: da }, , { value: mo }] = dtf.formatToParts(data);
  if (type == "dia") {
    return da;
  } else {
    return mo;
  }
};
const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
  <div
    {...props}
    className={
      "slick-nav prev-arrow" + (currentSlide === 0 ? " slick-disabled" : "")
    }
  >
    <i></i>
    <svg>
      <use xlinkHref="#circle" />
    </svg>
  </div>
);
const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => (
  <div
    {...props}
    className={
      "slick-nav next-arrow" +
      (currentSlide === slideCount - 1 ? " slick-disabled" : "")
    }
  >
    <i></i>
    <svg>
      <use xlinkHref="#circle" />
    </svg>
  </div>
);

export default class NoticiaComponent extends React.Component {
  state = {
    data: this.props.conteudo.data,
    totalpages: this.props.conteudo.pages,
    destaques: this.props.conteudo.destaque,
    page: 6,
    loadingScroll: false,
    loadingPage: 6,
  };
  componentDidMount() {
    this.setState({ width: window.innerWidth });
    window.addEventListener("scroll", this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }
  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };
  loadCategoria = async () => {
    if (this.state.page < this.state.totalpages * 6 && this.state.loadingPage === this.state.page) {
      fetch(
        `${process.env.urlApi}api/noticias?start=${this.state.page}&limit=6`
      )
        .then((res) => res.json())
        .then((conteudo) => {
          this.setState({
            data: this.state.data.concat(conteudo.data),
            page: this.state.page + 6,
            loadingScroll: false,
          });
        })
        .catch((e) => {
          console.log(e);
        });
      this.setState({ loadingPage: this.state.page + 6 });
    } else {
      this.setState({ loadingScroll: false });
    }
  };
  handleScroll = () => {
    const windowHeight =
      "innerHeight" in window
        ? window.innerHeight
        : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight =
      Math.max(
        body.scrollHeight,
        body.offsetHeight,
        html.clientHeight,
        html.scrollHeight,
        html.offsetHeight
      ) - 80;
    const windowBothrefm = windowHeight + window.pageYOffset;
    if (windowBothrefm >= docHeight) {
      if (this.state.page < 19) {
        this.setState({ loadingScroll: true });
        this.loadCategoria();
      }
    }
  };
  render() {
    const { data, loadingScroll, destaques } = this.state;

    return (
      <div className="main-content">
        <section className="container-fluid">
          <main id="main" role="main" style={{ marginTop: "5vw" }}>
            <div className="container bb-featured-area boxed item-1-4 badges-on">
              <div className="featured-area-wrapper">
                {destaques.map((conteudo, indexSections) => (
                  <div
                    className={`f-col col${indexSections + 1}`}
                    key={conteudo.id}
                  >
                    <article
                      className="featured-item"
                      style={{ backgroundImage: `url(${conteudo.img})` }}
                    >
                      <figure className="featured-media">
                        <a
                          href={`/post/${conteudo.link}`}
                          className="featured-link"
                        >
                          <img
                            src={conteudo.img}
                            alt={conteudo.titulo}
                            title={conteudo.titulo}
                          />
                        </a>
                      </figure>
                      <header className="featured-header">
                        <div className="featured-caption">
                          <div className="post-meta bb-post-meta">
                            <span className="post-meta-item post-views">
                              <span className="bb-icon bb-ui-icon-eye-1" />
                              <span className="count">2.8k</span>
                            </span>
                          </div>
                          <h2 className="entry-title">
                            <a href={`/post/${conteudo.link}`} rel="bookmark">
                              {conteudo.titulo}
                            </a>
                          </h2>
                        </div>
                      </header>
                      <div className="featured-badge-list">
                        <div className="bb-badge-list">
                          <a
                            className="bb-badge badge category category-20"
                            href="#"
                          >
                            <span className="circle">
                              <img
                                src="https://img.icons8.com/plasticine/2x/anime-emoji--v2.png"
                                alt="Sendo um Otaku"
                              />
                            </span>
                          </a>
                        </div>
                      </div>
                    </article>
                  </div>
                ))}
              </div>
              <div className="row mt-5">
                <div className="col-xl-8  mt-5">
                  <div className="row">
                    {
                      data.map(noticia => (

                        <div className="col-sm-4 post-item" key={noticia.id}>
                          <article className="post bb-post bb-card-item post-33042 type-post status-publish format-standard has-post-thumbnail category-indicacoes reaction-amei reaction-kimochi">
                            <div className="post-thumbnail">
                              <div className="bb-badge-list">
                                <a className="bb-badge badge category category-25" href="https://otakubfx.com.br/categoria/indicacoes/" title="Indicações"><span className="circle">
                                  <img src="https://i0.wp.com/otakubfx.com.br/wp-content/uploads/2020/03/indicacao-2.png?resize=150%2C150&ssl=1" alt="Indicações" />
                                </span>
                                  <span className="text">Indicações</span></a>
                              </div>
                              <a href={`/post/${noticia.link}`} title={noticia.titulo}> <div className="bb-media-placeholder" style={{ paddingBottom: '75%' }}>
                                <img src={noticia.img} alt={noticia.titulo} className="attachment-boombox_image360x270 size-boombox_image360x270 wp-post-image imgpost" />
                              </div>
                              </a>
                            </div>
                            <div className="content">
                              <header className="entry-header">
                                <h2 className="entry-title">
                                  <a href={`/post/${noticia.link}`} rel="bookmark">{noticia.titulo}</a>
                                </h2>
                              </header>
                            </div>
                          </article>
                        </div>

                      ))
                    } </div>
                </div>
                <div className="col-xl-4 mt-5 text-center">
                  <Anuncio.Amazon6 />
                </div>
              </div>
            </div>

            <div className="container main-container">

              {loadingScroll && (
                <div
                  style={{
                    width: "100%",
                    height: "100",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Loader
                    type="ThreeDots"
                    color="#2BAD60"
                    height={100}
                    width={100}
                  />
                </div>
              )}
            </div>
          </main>
        </section>
        <Head>
          <title>Notícias - Anime Fox</title>
          <meta property="og:url" content="http://animefox.me/" />
          <meta property="og:type" content="article" />
          <meta property="og:title" content="Anime Fox" />
          <meta
            property="og:description"
            content="Assista Naruto Shippuden, Attack on Titan, Dragon Ball Super, e muito mais! Acompanhe suas séries favoritas, com legendas profissionais em português. Anime. Novos Episódios do Japão. Mais de 1000 Séries. Legendas em Português."
          />
          <meta
            property="og:image"
            content="http://animefox.me/images/ogimage.jpg"
          />
        </Head>
      </div>
    );
  }
}

import React, { Component } from 'react'
import Link from '../../utils/ActiveLink';
import Api from "../../services/Api";
import Loader from 'react-loader-spinner'
import BuscaBar from './busca';

export default class Header extends Component {
    state = {
        query: '',
        results: [],
        loading: false,
        message: '',
        openBusca: false
    }

    handleOnInputChange = (event) => {
        const query = event.target.value;
        if (query.length > 3) {
            this.setState({ query, loading: true, message: '' }, () => {
                this.fetchSerachResult(query);
            })
        } else {
            this.setState({ query }, () => { })
        }

    };

    opBusca = () => {
        this.setState({ openBusca: true })
    }
    opBuscaClose = () => {
        this.setState({ openBusca: false })
    }

    fetchSerachResult = async (query) => {
        const responseBusca = await Api.BuscaService.getBusca(query);
        this.setState({ results: responseBusca.busca, loading: false, }, () => { })
    }

    render() {
        const { query, results, loading, openBusca } = this.state;
        return (
            <header id="main-header">
                <div className="main-header">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-12">
                                <nav className="navbar navbar-expand-lg navbar-light p-0">
                                    <a href="#" className="navbar-toggler c-toggler" >
                                        <div className="navbar-toggler-icon" data-toggle="collapse">
                                            <span className="navbar-menu-icon navbar-menu-icon--top" />
                                            <span className="navbar-menu-icon navbar-menu-icon--middle" />
                                            <span className="navbar-menu-icon navbar-menu-icon--bottom" />
                                        </div>
                                    </a>
                                    <a className="navbar-brand" href="/"> <img className="img-fluid logo" src="/images/logo.png" alt="Anime Fox" /> </a>
                                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                        <div className="menu-main-menu-container">
                                            <ul id="top-menu" className="navbar-nav ml-auto">
                                                <li className="menu-item">
                                                    <Link activeClassName="active" href="/" >
                                                        <a href="/">Início</a>
                                                    </Link>
                                                </li>
                                                <li className="menu-item">
                                                    <Link activeClassName="active" too="categoria" href="/categoria">
                                                        <a href="/categoria">Categoria</a>
                                                    </Link>
                                                </li>
                                                <li className="menu-item">
                                                    <Link activeClassName="active" too="filmes" href="/filmes">
                                                        <a href="/filmes">Filmes</a>
                                                    </Link>
                                                </li>
                                                <li className="menu-item">
                                                    <Link activeClassName="active" too="post" href="/noticias">
                                                        <a href="/noticias">Notícias</a>
                                                    </Link>
                                                </li>
                                                <li className="menu-item">
                                                    <Link activeClassName="active" href="/dcma">
                                                        <a href="/dcma">DCMA</a>
                                                    </Link>
                                                </li>
                                                <li className="menu-item">
                                                    <a href="https://www.cvv.org.br/" target="_blank">CVV</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="mobile-more-menu">
                                        <a className="more-toggle" id="dropdownMenuButton" data-toggle="more-toggle" aria-haspopup="true" aria-expanded="false">
                                            <i className="ri-more-line" />
                                        </a>
                                        <div className="more-menu" aria-labelledby="dropdownMenuButton">
                                            <div className="navbar-right position-relative">
                                                <ul className="d-flex align-items-center justify-content-end list-inline m-0">
                                                    <li>
                                                        <a href="#" onClick={() => { this.opBusca() }}>
                                                            <i className="ri-search-line" />
                                                        </a>
                                                        {openBusca ? <BuscaBar handleOnInputChange={this.handleOnInputChange} results={results} loading={loading} query={query} close={this.opBuscaClose} /> : null}
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="navbar-right menu-right">
                                        <ul className="d-flex align-items-center list-inline m-0">
                                            <li className="nav-item nav-icon">
                                                <a href="#" onClick={() => { this.opBusca() }}>
                                                    <i className="ri-search-line" />
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                    {openBusca ? <BuscaBar handleOnInputChange={this.handleOnInputChange} results={results} loading={loading} query={query} /> : null}

                                </nav>
                                <div className="nav-overlay" />

                            </div>
                        </div>
                    </div>
                </div>
            </header >
        )
    }
}
import React from 'react'
import Loader from 'react-loader-spinner'

function useOutsideAlerter(ref, close) {
    React.useEffect(() => {

        function handleClickOutside(event) {
            if (!ref.current.contains(event.target) && !event.target.className.includes('search')) {
                document.removeEventListener("click", handleClickOutside);
                close()
            }
        }

        // Bind the event listener
        document.addEventListener("click", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("click", handleClickOutside);
        };
    }, [ref]);
}


const BuscaBar = (props) => {
    const { results, query, loading } = props;

    const searchInput = React.useRef(null);
    const wrapperRef = React.useRef(null);

    React.useEffect(()=>{
       // current property is refered to input element
       searchInput.current.focus();
    },[])

    useOutsideAlerter(wrapperRef, props.close);


    return <div ref={wrapperRef} className="search-box iq-search-bar d-search">
        <form  className="searchbox">
            <div className="position-relative">
                <input type="text" className="text search-input font-size-12"
                    placeholder="Digite aqui para buscar..."
                    onChange={props.handleOnInputChange}
                    value={query}
                    ref={searchInput}
                />
                <i className="search-link ri-search-line" />
            </div>
        </form>
        {!loading && results.length != 0 && <div className="search-result">
            {!loading && results.length != 0 && <div  className="search-title"> Resultados encontrados para: {query}</div>}
            {!loading && results.map((i, index) => (
                <a href={`/anime/${i.url}`} key={index}>
                    <div className="d-flex flex-row" style={{ cursor: 'pointer' }}>

                        <img className="result-image" src={`https://image.tmdb.org/t/p/w185/${i.img}`} />
                        <span className="d-flex flex-column">
                            <h5 className="result-title">{i.nome}</h5>
                            <p className="result-date">{i.ano}</p>
                        </span>

                    </div>
                </a>
            ))
            }
            {loading && <div style={{ width: "100%", height: "100", display: "flex", justifyContent: "center", alignItems: "center" }} >
                <Loader type="ThreeDots" color="#2BAD60" height={100} width={100} />
            </div>}
        </div>}
    </div>
}


export default BuscaBar
import React, { Component } from 'react'


export default class Header extends Component {
    render() {
        return (

            <footer className="mb-0">
                <div className="container-fluid">
                    <div className="block-space">
                        <div className="row">
                            <div className="col-lg-3 col-md-4">
                                <ul className="f-link list-unstyled mb-0">
                                    <li><a href="/">Inicio</a></li>
                                    <li><a href="/categoria">Categoria</a></li>
                                    <li><a href="/noticias">Notícias</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <ul className="f-link list-unstyled mb-0">
                                    <li><a href="/privacy">Política de Privacidade</a></li>
                                    <li><a href="/termos">Termos &amp; Condições</a></li>
                                    <li><a href="/dcma">DCMA</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-3 col-md-4">
                                <ul className="f-link list-unstyled mb-0">
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Contato</a></li>
                                </ul>
                            </div>
                            <div className="col-lg-3 col-md-12 r-mt-15">
                                <div className="d-flex">
                                    <a href="https://www.facebook.com/Anime-Fox-110231950651473/"  target="_blank" className="s-icon">
                                        <i className="ri-facebook-fill" />
                                    </a>
                                    <a href="#" className="s-icon">
                                        <i className="ri-instagram-fill" />
                                    </a>
                                    <a href="https://discord.gg/qNd2NV2" target="_blank" className="s-icon">
                                        <i className="ri-discord-fill" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="copyright py-2">
                    <div className="container-fluid">
                        <p className="mb-0 text-center font-size-14 text-body">Anime Fox - 2020 All Rights Reserved</p>
                    </div>
                </div>
            </footer>
        );
    }
};
import React, { useEffect, useState } from 'react';
import Head from 'next/head'
import Player from '../player'
import Episodios from './episodios'
import NotFound from '../notfound'
import {
    EmailIcon,
    FacebookIcon,
    TelegramIcon,
    TwitterIcon,
    WhatsappIcon,
    EmailShareButton,
    FacebookShareButton,
    TelegramShareButton,
    TwitterShareButton,
    WhatsappShareButton,

} from "react-share";
import { AGrande } from '../Anuncios/anuncions'

export default function Anime(props) {
    const [content, setContent] = useState(props.data.res);
    const [isFetching, setIsFetching] = useState(false);
    const [temporada, setTemporada] = useState(1);
    const [episodio, setEpisodio] = useState(props.data.episodio);
    const [play, setPlay] = useState(false);
    const [lEP, setLast] = useState(null);
    const [fristEp, setfristEp] = useState(null);

    useEffect(() => {
        async function init() {
            try {
                if (props.data.episodio !== null) {
                    setPlay(true)
                }
                setLast(localStorage.getItem(`${content.url}`) != null ? JSON.parse(localStorage.getItem(`${content.url}`)) : null)

            } catch (e) {
                // console.log(e)
            } finally {
                setIsFetching(false);
            }
        }

        init();
    }, []);

    const selectEpisodio = (episodio) => {
        if (episodio == null) {
            episodio = fristEp
        }
        setEpisodio(episodio)
        setPlay(true)
    }

    const handleChange = () => {
        setPlay(false)
    }

    if (content !== null) {
        var temporadas = []
        //Temporadas de animes
        for (let i = 1; i <= content.anime_temporadas; i++) {
            temporadas.push(<option value={i} key={i} >{i}ª Temporada</option>)
        }
        return <div>
            {!play &&
                <section className="banner-wrapper overlay-wrapper" style={{ paddingTop: '40vw', backgroundImage: `url('http://image.tmdb.org/t/p/original${content.anime_bg}')` }}>
                    <div className="banner-caption">
                        <div className="position-relative mb-4">
                            <a onClick={() => selectEpisodio(lEP)} className="d-flex align-items-center" style={{ cursor: 'pointer' }}>
                                <div className="play-button">
                                    <i className="fa fa-play" />
                                </div>
                                <h4 className="w-name text-white font-weight-700">{typeof window != 'undefined' ? localStorage.getItem(`${content.url}`) === null ? 'Assistir Agora' : 'Continuar Assistindo' : 'Assistir Agora'}</h4>
                            </a>
                        </div>
                        <ul className="list-inline p-0 m-0 share-icons music-play-lists">
                            <li><span><i className="ri-add-line" /></span></li>
                            <li><span><i className="ri-heart-fill" /></span></li>
                            <li className="share">
                                <span><i className="ri-share-fill" /></span>
                                <div className="share-box">
                                    <div className="d-flex align-items-center">
                                        <FacebookShareButton title={`${content.anime_nome} - Assista no Anime Fox`} url={`http://animefox.me/anime/${content.url}`} children={<FacebookIcon size={32} round={true} />} />
                                        <TwitterShareButton title={`${content.anime_nome} - Assista no Anime Fox`} url={`http://animefox.me/anime/${content.url}`} children={<TwitterIcon size={32} round={true} />} />
                                        <TelegramShareButton title={`${content.anime_nome} - Assista no Anime Fox`} url={`http://animefox.me/anime/${content.url}`} children={<TelegramIcon size={32} round={true} />} />
                                        <WhatsappShareButton title={`${content.anime_nome} - Assista no Anime Fox`} url={`http://animefox.me/anime/${content.url}`} children={<WhatsappIcon size={32} round={true} />} />
                                        <EmailShareButton title={`${content.anime_nome} - Assista no Anime Fox`} url={`http://animefox.me/anime/${content.url}`} children={<EmailIcon size={32} round={true} />} />
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
            }
            <div className="main-content">
                {!play && <div>
                    <section className="movie-detail container-fluid">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="trending-info g-border">
                                    <h1 className="trending-text big-title text-uppercase mt-0">{content.anime_nome}</h1>
                                    <ul className="p-0 list-inline d-flex align-items-center movie-content">
                                        {content.categoria !== undefined && content.categoria.map((categoria, index) => <li key={index} className="text-white">{categoria.categoria_nome}</li>)}
                                    </ul>
                                    <div className="d-flex align-items-center text-white text-detail">
                                        <span >{content.anime_situacao}</span>
                                        <span className="trending-year">{content.anime_ano}</span>
                                    </div>
                                    <p className="trending-dec w-100 mb-0" dangerouslySetInnerHTML={{ __html: content.anime_sinopse }}></p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <AGrande id={0} />
                    <section className="container-fluid seasons">
                        <div className=" d-inline-block sea-epi s-margin">
                            <select name="temporadas" className="select-css"
                                value={temporada}
                                onChange={e => { setTemporada(e.target.value) }}>
                                {temporadas}
                            </select>
                        </div>
                        <ul className="trending-pills d-flex nav nav-pills align-items-center text-center s-margin" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active show" data-toggle="pill" href="#episodes" role="tab" aria-selected="true">Episódios</a>
                            </li>
                        </ul>
                        <div className="tab-content">
                            <Episodios temporada={temporada} anime={content.anime_id} onSelectEpisodio={selectEpisodio} fristEp={setfristEp} />
                        </div>
                    </section>
                </div>}
                {play && <section className="playerAnime">
                    <Player episodio={episodio} animeNome={content.anime_nome} animeId={content.anime_id} url={content.url} onBack={handleChange} />
                </section>}
                <AGrande id={1} />
            </div>
            {props.data.episodio !== null && (<Head>
                <title>{` Assistir ${content.anime_nome} - Episódio ${props.data.episodio.numero}`}</title>
                <meta property="og:url" content={`http://animefox.me/anime/${content.url}`} />
                <meta property="og:type" content="article" />
                <meta property="og:title" content={` Assistir ${content.anime_nome} - Episódio ${props.data.episodio.numero}`} />
                <meta property="og:description" content={`${props.data.episodio.descricao}`} />
                <meta property="og:image" content={`${props.data.episodio.img}`} />

                <meta property="og:site_name" content={` Assistir ${content.anime_nome} - Episódio ${props.data.episodio.numero}`} />
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:description" content={`${props.data.episodio.descricao}`} />
                <meta name="twitter:title" content={` Assistir ${content.anime_nome} - Episódio ${props.data.episodio.numero}`} />
            </Head>)}
            {props.data.episodio === null && (<Head>
                <title>{`${content.anime_nome} - Assista no Anime Fox`}</title>
                <meta property="og:url" content={`http://animefox.me/anime/${content.url}`} />
                <meta property="og:type" content="article" />
                <meta property="og:title" content={`${content.anime_nome} - Assista no Anime Fox`} />
                <meta property="og:description" content={`${content.anime_sinopse}`} />
                <meta property="og:image" content={`http://image.tmdb.org/t/p/w780${content.anime_bg}`} />

                <meta property="og:site_name" content={`${content.anime_nome} - Assista no Anime Fox`} />
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:description" content={`${content.anime_sinopse}`} />
                <meta name="twitter:title" content={`${content.anime_nome} - Assista no Anime Fox`} />
            </Head>)}
        </div>
    } else {
        return <NotFound statusCode={404} />

    }

}
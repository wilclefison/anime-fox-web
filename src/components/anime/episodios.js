import React, { useEffect, useState } from 'react';
import Api from "../../services/Api";
import Loader from 'react-loader-spinner';



export default function Episodios(props) {
    const [isFetching, setIsFetching] = useState(false);
    const [eps, setEps] = useState(null);


    useEffect(() => {
        init();
    }, [props.temporada]);

    async function init() {
        try {
            setIsFetching(true);
            const resEpis = await Api.EpisodeService.getEpiTemp(props.anime, props.temporada);
            setEps(resEpis);
            props.fristEp(resEpis[0])
        } catch (e) {
            console.log(e)
        } finally {
            setIsFetching(false);
        }
    }

    if (eps !== null && isFetching == false) {
        return (
            <div className="tab-pane fade active show"  >
                <div className="block-space">
                    <div className="row">
                        {eps.map((ep, index) => (
                            <div className="col-1-5 col-md-6 iq-mb-30" key={ep.id}>
                                <div className="epi-box">
                                    <div className="epi-img position-relative">
                                        <img src={ep.img} className="img-fluid img-zoom" alt="" />
                                        <div className="episode-number">{ep.numero}</div>
                                        <div className="episode-play-info">
                                            <div className="episode-play">
                                                <span onClick={() => props.onSelectEpisodio(ep)}>
                                                    <i className="ri-play-fill" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="epi-desc p-3">
                                        <div className="d-flex align-items-center justify-content-between">
                                            <span className="text-white">{new Date(ep.air_date).toLocaleDateString("pt-BR")}</span>
                                            <span className="text-primary">{ep.duracao}m</span>
                                        </div>
                                        <a style={{ cursor: 'pointer' }} onClick={() => props.onSelectEpisodio(ep)}>
                                            <h6 className="epi-name text-white mb-0">{ep.nome}</h6>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        ))}

                    </div>
                </div>
            </div>
        )
    }

    return <div style={{ width: "100%", height: "100", display: "flex", justifyContent: "center", alignItems: "center" }} >
        <Loader type="ThreeDots" color="#2BAD60" height={100} width={100} />
    </div>
}



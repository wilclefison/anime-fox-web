import { withRouter } from 'next/router';
import Link from 'next/link';
import React, { Children } from 'react';

const ActiveLink = ({ router, children, ...props }) => {
  const child = Children.only(children);

  let className = child.props.className || '';
  if (router.pathname === props.href && props.activeClassName) {
    className = `${className} ${props.activeClassName}`.trim();
  }

  if (props.too && props.too && router.pathname.includes(props.too)) {
    className = `${className} ${props.activeClassName}`.trim();
  }

  delete props.too;
  delete props.activeClassName;

  return React.cloneElement(child, { className });
};

export default withRouter(ActiveLink);
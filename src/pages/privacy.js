import Layout from '../components/Layout'
import Head from 'next/head'

const Index = () =>
    <Layout>
         <div className="main-content">
            <section className="container-fluid">
                    <div className="dmcCa">
                        <h1 className="minh1">PRIVACY POLICY</h1>
                        <h2 className="minh2">Política de Privacidade</h2>
                        <p>A Política de Privacidade do Anime Fox, em conjunto com Termos de serviço e outros termos e condições de uso estão aqui incorporados por referência e podem fazer menções a serviços específicos, regem o uso do site Anime Fox e são definidos coletivamente como "Contrato".</p>
                        <p>Os termos podem ser alterados, entrando em vigor imediatamente após a publicação no site. Os usuários serão notificados sobre os novos termos e o uso continuado do site e dos serviços associados implicam em sua aceitação.</p>
                        <p>O Anime Fox utiliza endereços de IP para medir o uso, criar estatísticas, coletar informações e administrar o site.</p>
                        <p>Cookies são pequenos arquivos de dados que contém informações sobre você e são armazenados em seu disco rígido. Os cookies identificam-no para nosso banco de dados, mas não fornecem nenhuma informação além das que você eventualmente já tenha fornecido ao Anime Fox em um suposto ato de cadastro ou em um preenchimento de formulários de contato. Os cookies também são capazes de ajudar o Anime Fox a acompanhar e direcionar seus interesses para melhorar a sua experiência em nosso site.</p>
                        <p>Caso seja um usuário cadastrado, você pode rever, atualizar, corrigir ou apagar informações pessoais ou sua conta no painel de controle, na página "Minha Conta".</p>
                        <p>O endereço de e-mail fornecido no ato do registro ou preenchimento de formulário de contato, será utilizado para enviar informações e atualizações referentes à sua ordem.</p>
                        <p>Teremos o direito de nos comunicar com o cliente ou empresa do cliente através do e-mail fornecido no ato do cadastro ou preenchimento de formulário de contato. Entretanto, não serão enviados e-mails de conteúdo publicitário caso não haja consentimento do cliente.</p>
                        <p>Não iremos alugar, vender ou compartilhar suas informações com terceiros. No entanto, podemos divulgar suas informações pessoais ou quaisquer informações de seus arquivos ou históricos (logs) quando exigido por lei ou na crença da boa-fé de que tais ações são necessárias para cumprir os ditames da lei ou cumprir com um processo legal servido em nós; proteger e defender os direitos de propriedade do Anime Fox, seus visitantes ou usuários, identificar pessoas que podem estar violando a lei, o aviso prévio legal, ou os direitos de terceiros; cooperar com as investigações de supostas atividades ilegais.</p>
                        <p>Sites de terceiros e serviços ligados ao Anime Fox não são cobertos por esta Política de Privacidade.</p>
                        <p>Você é o único responsável por proteger o sigilo de sua senha, e o Anime Fox não se responsabilizará se você oferecer informações pessoais online que sejam acessíveis ao público. Neste caso seus dados poderiam ser usados de uma maneira que você viole a lei, a sua privacidade pessoal ou a sua segurança. Ao enviar essa informação ou compartilhá-la através de comentários e outros meios de comunicação público ou até mesmo privado, quando não sob controle do próprio Anime Fox, você assume os riscos e todas as responsabilidades que possam surgir com o resultado de tais informações se tornarem públicas.</p>
                        <p>No caso do Anime Fox ser adquirido ou se fundir com uma entidade terceira, reservamos o direito de transferir ou ceder as informações que coletamos de você como parte de qualquer mudança de controle.</p>

                    </div>
            
            </section>
            <Head>
                <title>{`Anime Fox - Política de Direitos Autorais`}</title>
            </Head>
        </div>
    </Layout>

export default Index
import Layout from '../components/Layout'
import Head from 'next/head'

const Index = () =>
    <Layout>
        <div className="main-content">
            <section className="container-fluid">
                <div className="dmcCa">
                    <h3>TERMOS DE USO:</h3>
                    <br />
                    <div>O <strong  style={{color: '#f4b033'}}>Anime Fox</strong> respeita os termos da DMCA (Digital Millennium Copyright Act [Lei dos Direitos Autorais do Milênio Digital])</div>
                    <div>O DMCA (Digital Millennium Copyright Act) foi aprovado no Congresso dos EUA em 1998 para tratar de violação de direitos autorais em meios eletrônicos, particularmente na Internet.</div>
                    <div>E que quando é solicitada um pedido de remoção de algum conteúdo que viola os termos, e que o autor não deseja que ele permaneça sendo distribuído de forma online através do sistema de streaming de nosso site.</div>
                    <div>Nós respeitamos a solicitação e fazemos sempre a devida remoção.</div><br />
                    <div>Nosso único objetivo é o compartilhamento de cultura, pois nossa intenção não é de sermos substitutos de produtos originais, o <strong  style={{color: '#f4b033'}}>www.animefox.me</strong> funciona como amostra grátis. Mesmo sabendo que alguns usuários possam violar nossos termos, não nos responsabilizamos pelo mau uso dos mesmos. Pois o website foi desenvolvido para que pessoas que não tivessem conhecimento de determinado conteúdo pudesse assistí-lo de forma gratuita e caso houvesse interesse, ela poderia adquirir o produto original sem medo de arrependimento. Nós acreditamos que uma obra tenha um maior valor quando atinge a um maior público. E nosso trabalho com o site é exatamente este.</div>
                    <br /><div>Como já dito, o <strong  style={{color: '#f4b033'}}> www.animefox.me,</strong> não se responsabiliza pelo mau uso de seus usuários. Todavia, esperamos que os mesmos sejam éticos de respeitar nossos termos. Nós não recebemos valor pelas obras autorais, os valores são gerados para pagar custos com servidores. Todos os links de animes disponíveis no www.animefox.me, foram organizados a partir de sites de terceiros. Nosso site assim como o google.com, apenas compartilha os links encontrados, porém, o www.animefox.me, disponibiliza através de serviço streaming. Um serviço gratuito para todos os usuários.</div>
                    <div>O www.animefox.me, não se caracteriza empresa, mas apenas um portal físico na internet que disponibiliza de forma gratuita “como amostra grátis”, exibição de animes, para incentivar as pessoas a adquirirem produtos originais.</div>
                    <div />
                    <div /><br />
                    <div>Se você gostou de algum conteúdo visualizado aqui no site, RECOMENDAMOS que vocês façam a compra do MESMO! ADQUIRIR PRODUTOS ORIGINAIS É EXTREMAMENTE IMPORTANTE!</div>
                    <div />
                    <div /><br />
                    <div>PRODUTOS ORIGINAIS, SÃO VENDIDOS PARA QUE AS INDÚSTRIAS CINEMATOGRÁFICAS POSSAM TER SEUS LUCROS GERADO COM BASE NOS INVESTIMENTOS DADO AO ANIME. SEM ESTES INVESTIMENTOS, MUITAS PRODUTORAS ACABARIAM POR DESISTIR DE DESENVOLVER UM DETERMINADO ANIME! ENTÃO JAMAIS DEIXE DE ADQUIRIR OS PRODUTOS ORIGINAIS!</div>
                    <div />
                    <div /><br />
                    <div>Acesse alguma das lojas abaixo e realize a compra.</div>
                    <div>– <a href="http://www.americanas.com.br/">www.americanas.com.br</a></div>
                    <div>– <a href="http://www.submarino.com.br/">www.submarino.com.br</a></div>
                    <div>– <a href="http://www.fnac.com.br/">www.fnac.com.br</a></div>
                    <div>– <a href="http://www.extra.com.br/">www.extra.com.br</a></div>
                    <div>– <a href="http://www.livraria.folha.com.br/">www.livraria.folha.com.br</a></div>
                    <div>– <a href="http://www.saraiva.com.br/">www.saraiva.com.br</a></div>
                    <div>– <a href="http://www.shopping.uol.com.br/">www.shopping.uol.com.br</a></div>
                    <div>Caso prefira, é possível assinar um serviço de transmissão de vídeo, onde grande parte dos lucros é direcionado para as produtoras de animes. O nome da companhia é "Crunchyroll", para assinar e ganhar 14 dias grátis para utilização, <a href="https://www.crunchyroll.com/pt-br/freetrial">Clique aqui!</a></div>
                    <div />
                    <div /><br />
                    <h3>Ao acessar o site você concorda que:</h3>
                    <ul>
                        <li>Eu concordo que estarei usando o site apenas para o conhecimento da obra e posteriormente realizarei a compra do produto original, pois entendo que a reprodução ilegal de animes prejudicam o mercado das indústrias cinematográficas.</li>
                        <li>Eu concordo que o site www.animefox.me, serve apenas como um divulgador de conteúdo e incentivo a cultura.</li>
                        <li>Ao acessar o site você aceita que utilizemos COOKIES para obter uma melhor experiência de navegação para o usuário.</li>
                    </ul>
                    <div><a href="https://www.animefox.me/" style={{color: '#f4b033'}}>Eu aceito os termos</a> | <a href="https://www.google.com/">Não aceito</a></div>
                    <br /><br />
                    <p>Atenciosamente,<br />
          Equipe Anime Fox
        </p>
                </div>


            </section>
            <Head>
                <title>{`Anime Fox - Termos de Uso`}</title>
            </Head>
        </div>
    </Layout >

export default Index
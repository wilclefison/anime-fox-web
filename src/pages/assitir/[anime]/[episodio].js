import { useRouter } from 'next/router'
import Layout from '../../../components/Layout'
import Anime from '../../../components/anime'
import Api from "../../../services/Api";


const anim = (data) => {
  const router = useRouter()
  return <Layout>
    <Anime data={data} />
  </Layout>
}

anim.getInitialProps = async ({ query }) => {
  const { anime, episodio } = query
  try {
    const res = await Api.AnimeService.getAnime(anime);
    const resEp = await Api.EpisodeService.getEpisode(episodio);
    return { res: res[0], anime, episodio: resEp[0] };
  } catch (error) {
    return { res: null, anime, episodio: null };
  }
  return { anime, episodio };
};

export default anim
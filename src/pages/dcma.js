import Layout from '../components/Layout'
import Head from 'next/head'

const Index = () =>
    <Layout>
        <div className="main-content">
            <section className="container-fluid">
                <div className="dmcCa">
                    <h2 className="minh1">Política de Direitos Autorais</h2>
                    <br /><p><strong>A Animefox.me respeita todos os direitos autorais e seus proprietários e adere à Lei dos Direitos Autorais do Milênio Digital (DMCA).</strong></p>
                    <br /><p>Lembrando que todas as obras que estão disponiveis são a pedido de fans, então nossa AI (Inteligencia Artifical) criada para buscar informações das obras na internet pega informações e traz para o nosso site.</p>
                    <br /> <ul>
                        <li>Se você for dono de conteúdo e gostaria que ajudássemos a distribuí-lo, entre em contato com o email contato@animefox.me.</li>
                        <li>Se você gostaria de ter seu conteúdo removido deste site, siga as instruções abaixo.</li>
                    </ul>
                    <p>
                        Para registrar uma notificação de violação de direitos autorais junto à Animefox, envie um comunicado em escrito que contenha substancialmente o seguinte (consulte o seu advogado ou ver a Seção 512(c)(3) do DMCA para confirmar os requisitos):
  </p><br />
                    <ol>
                        <li>
                            Uma assinatura física ou eletrônica de uma pessoa autorizada para agir em nome do proprietário de um direito exclusivo que teria sido violado.
  </li>
                        <li>
                            Identificação da obra cujos direitos autorais foram violados ou, no caso de vários trabalhos terem sido encontrados em um único site e forem abrangidos em uma única notificação, uma lista representativa desses trabalhos.
  </li>
                        <li>
                            Identificação do material que esteja alegadamente sendo violado, violou ou que seja o objeto de uma atividade ilegítima e deve ser removido ou tenha seu acesso desabilitado, assim como informações suficientes para permitir que o provedor de serviços localize o material. link
                            <b>Fornecer URLs no corpo do e-mail é a melhor maneira de nos ajudar a localizar o conteúdo rapidamente.</b>
                        </li>
                        <li>
                            Informações suficientes para permitir ao provedor de serviços localizar a parte reclamante, tal como um endereço, número de telefone e, se disponível, um endereço de e-mail pelo qual a parte reclamante possa ser localizada.
  </li>
                        <li>
                            Uma declaração no sentido que a parte reclamante tenha o entendimento de boa fé que o uso do material do modo que está sendo reclamado não foi autorizado pelo proprietário dos direitos autorais, seu representante ou pela lei.
  </li>
                        <li>
                            Uma declaração de que as informações contidas na notificação são precisas e, sob pena de perjúrio, que a parte reclamante está autorizada a agir em nome do proprietário de um direito exclusivo que supostamente esteja sendo violado.
  </li>
                    </ol>
                    <p>
                        Essa notificação deverá ser enviada para nosso representante do seguinte modo:<br /><br />
    DMCA Complaints<br />Animefox.me<br />E-mail: copyright@animefox.me<br /><br /><br />
    Por favor, note que sob a Seção 512(f) qualquer pessoa que alegar consciente e falsamente a violação dos direitos ou atividade ilegal desses materiais poderá ser responsabilizado civilmente.
  </p><br />
                </div>


            </section>
            <Head>
                <title>{`Anime Fox - Política de Privacidade`}</title>
            </Head>
        </div>
    </Layout >

export default Index
import Layout from '../../components/Layout'
import Post from '../../components/news/post'
import Api from "../../services/Api";

const post = (data) => {


  return <Layout>
    <Post data={data.res} />
  </Layout>

}

post.getInitialProps = async ({ query }) => {
  const { nome } = query;
  try {
    const res = await Api.NewsService.getNews(nome);
    return { res };
  } catch (error) {
    return { res: null };
  }
};


export default post
import Layout from '../components/Layout'
import HomeSlider from '../components/slides/home'
import ItensSlider from '../components/slides/itens'
import Api from "../services/Api";

import Head from 'next/head'

const Index = (data) =>
  <Layout>
    {/* Slider Start */}
    <section id="home" className="iq-main-slider p-0">
      <HomeSlider homeItens={data.homeSlide} />
      <svg xmlns="http://www.w3.org/2000/svg" style={{ display: 'none' }}>
        <symbol xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44" width="44px" height="44px" id="circle" fill="none" stroke="currentColor">
          <circle r={20} cy={22} cx={22} id="test" />
        </symbol>
      </svg>
    </section>
    <div className="main-content">
      <ItensSlider homeItens={data.homeItens} />
    </div>
    {/* Slider End */}
    <Head>
      <title>Anime Fox - Assista a Naruto Shippuden, Bleach e episódios de outros animes online e de graça</title>
      <meta property="og:locale" content="pt_BR" />
      <meta property="og:type" content="article" />
      <meta property="og:title" content="Anime Fox" />
      <meta property="og:description" content="Assista Naruto Shippuden, Attack on Titan, Dragon Ball Super, e muito mais! Acompanhe suas séries favoritas, com legendas profissionais em português. Anime. Novos Episódios do Japão. Mais de 1000 Séries. Legendas em Português." />
      <meta property="og:url" content="http://animefox.me/" />
      <meta property="og:image" content="http://animefox.me/images/ogimage.jpg" />
      <meta property="og:site_name" content="Anime Fox" />
      <meta name="twitter:card" content="summary" />
      <meta name="twitter:description" content="Assista Naruto Shippuden, Attack on Titan, Dragon Ball Super, e muito mais! Acompanhe suas séries favoritas, com legendas profissionais em português. Anime. Novos Episódios do Japão. Mais de 1000 Séries. Legendas em Português." />
      <meta name="twitter:title" content="Anime Fox" />
    </Head>
  </Layout>


Index.getInitialProps = async ({ query }) => {
  try {
    const responseSlides = await Api.HomeService.getSlides();
    const responseHomeItens = await Api.HomeService.getHome();
    return { homeSlide: responseSlides, homeItens: responseHomeItens };
  } catch (error) {
    return { homeSlide: [], homeItens: [] };
  }
};


export default Index
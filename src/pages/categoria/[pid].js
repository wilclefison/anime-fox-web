import { useRouter } from 'next/router'
import Layout from '../../components/Layout'
import AnimeCat  from '../../components/categorias/animeCat'

const CatCategoria = (data) => {
  const router = useRouter()
  return <Layout>
    <AnimeCat data={data} />
  </Layout>
}

CatCategoria.getInitialProps = async ({ query }) => {
  const { pid } = query
  const res = await fetch(`${process.env.urlApi}api/categoria/${pid}`)
  const json = await res.json()
  return { data: json, pid };
};
export default CatCategoria  
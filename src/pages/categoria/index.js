import Layout from '../../components/Layout'
import Categorias from '../../components/categorias'

const Index = (data) =>
        <Layout>
            <Categorias data={data.json} />
        </Layout>

Index.getInitialProps = async () => {
    const res = await fetch(`${process.env.urlApi}api/categoria`)
    const json = await res.json()
    return { json };
  };
export default Index


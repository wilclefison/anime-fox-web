import Layout from '../components/Layout'
import NotFound from '../components/notfound'

const noContent = () =>
    <Layout>
        <NotFound statusCode={500} />
    </Layout>

export default noContent
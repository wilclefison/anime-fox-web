import Layout from '../../components/Layout'
import NoticiaComponent from '../../components/news'
import fetch from 'isomorphic-unfetch'

const noticiaContent = (data) =>
    <Layout>
        <NoticiaComponent conteudo={data.json} />
    </Layout>
    
noticiaContent.getInitialProps = async () => {
    const res = await fetch(`${process.env.urlApi}api/noticias`)
    var json = await res.json()
    return { json };
};

export default noticiaContent
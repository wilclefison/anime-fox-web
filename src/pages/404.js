import Layout from '../components/Layout'
import NotFound from '../components/notfound'

const noContent = () =>
    <Layout>
        <NotFound statusCode={404} />
    </Layout>

export default noContent
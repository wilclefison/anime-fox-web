import React, { useEffect, useState } from 'react';
import Layout from '../../components/Layout'
import Anime from '../../components/anime'
import Api from "../../services/Api";

const anim = (data) => {
  return <Layout >
    <Anime data={data} />
  </Layout>

}

anim.getInitialProps = async ({ query }) => {
  const { id } = query;
  try {
    const res = await Api.AnimeService.getAnime(id);
    return { res: res[0], episodio: null };
  } catch (error) {
    return { res: null, episodio: null };
  }
};

export default anim
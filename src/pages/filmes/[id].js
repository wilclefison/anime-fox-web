import React, { useEffect, useState } from 'react';
import Layout from '../../components/Layout'
import Movie from '../../components/filmes/filme'
import Api from "../../services/Api";

const anim = (data) => {
  return <Layout >
    <Movie data={data} />
  </Layout>

}

anim.getInitialProps = async ({ query }) => {
  const { id } = query;
  try {
    const res = await Api.MovieService.getMovie(id);
    return { res: res[0], episodio: null };
  } catch (error) {
    return { res: null, episodio: null };
  }
};

export default anim
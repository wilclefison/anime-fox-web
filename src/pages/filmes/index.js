import Layout from '../../components/Layout'
import MovieSlider from '../../components/slides/movie'
import MovieList from '../../components/filmes'



const Index = (data) =>
    <Layout>
        <div>
            {/* Slider Start */}
            <section className="iq-main-slider p-0">
                <MovieSlider />
            </section>
            {/* Slider End */}
            {/* MainContent */}
            <MovieList data={data} />
        </div>

    </Layout>

Index.getInitialProps = async () => {
    const res = await fetch(`${process.env.urlApi}api/movie`)
    const json = await res.json()
    return { data: json, };
};
export default Index


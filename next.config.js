// next.config.js
const withCSS = require('@zeit/next-css')

module.exports = withCSS({
  env: {
    urlApi: 'https://api.animefox.me/',
  }
})